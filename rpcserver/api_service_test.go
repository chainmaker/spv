/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package rpcserver

import (
	"context"
	"testing"

	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/pb/api"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo/cm_pbgo"
	"chainmaker.org/chainmaker/spv/v2/server"
	"github.com/stretchr/testify/require"
)

func TestApiServer_Func(t *testing.T) {
	spvServer := &server.SPVServer{}
	err := spvServer.SetLog(logger.GetDefaultLogger())
	require.Nil(t, err)

	apiSrv := NewApiService(context.Background(), spvServer, logger.GetDefaultLogger())
	resp, err := apiSrv.ValidTransaction(context.Background(), &api.TxValidationRequest{})
	require.NotNil(t, resp)
	require.Nil(t, err)

	response, err := apiSrv.ForwardRequest(context.Background(), &cm_pbgo.TxRequest{})
	require.NotNil(t, response)
	require.Nil(t, err)
}
