/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package rpcserver

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"testing"

	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/server"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const (
	tlsCa = "../config/tls/ca.pem"

	tlsServerCert = "../config/tls/server.pem"
	tlsServerKey  = "../config/tls/server.key"

	tlsClientCert = "../tools/config/tls/client.pem"
	tlsClientKey  = "../tools/config/tls/client.key"
)

func TestRpcServer_Func(t *testing.T) {
	conf.SPVConfig.GRPCConfig = &conf.GRPCConfig{
		Address:   "localhost",
		Port:      12345,
		EnableTLS: true,
		Security: &conf.TransportSecurity{
			EnableCertAuth: false,
			CAFile: []string{
				tlsCa,
			},
			CertFile: tlsServerCert,
			KeyFile:  tlsServerKey,
		},
	}

	// server
	rpcSrv, err := NewRPCServer(&server.SPVServer{})
	require.Nil(t, err)
	err = rpcSrv.Start()
	require.Nil(t, err)
	defer rpcSrv.Stop()

	// client
	_, err = newRpcConnection("127.0.0.1:12345", true, false)
	require.Nil(t, err)

}

func newRpcConnection(grpcAP string, enableTls, enableCa bool) (*grpc.ClientConn, error) {
	// 双向认证
	if enableCa {
		//从证书相关文件中读取和解析信息，得到证书公钥、密钥对
		cert, err := tls.LoadX509KeyPair(tlsClientCert, tlsClientKey)
		if err != nil {
			fmt.Printf("grpc client read cert and key file failed, err:%v\n", err)
			return nil, nil
		}
		// 创建一个新的、空的 CertPool
		certPool := x509.NewCertPool()
		ca, err := ioutil.ReadFile(tlsCa)
		if err != nil {
			fmt.Printf("grpc client read ca file failed, err:%v\n", err)
			return nil, nil
		}
		//尝试解析所传入的 PEM 编码的证书。如果解析成功会将其加到 CertPool 中，便于后面的使用
		certPool.AppendCertsFromPEM(ca)
		//构建基于 TLS 的 TransportCredentials 选项
		cred := credentials.NewTLS(&tls.Config{ // nolint
			//设置证书链，允许包含一个或多个
			Certificates: []tls.Certificate{cert},
			//要求必须校验客户端的证书。可以根据实际情况选用以下参数
			ServerName: "localhost",
			RootCAs:    certPool,
		})
		return grpc.Dial(grpcAP, grpc.WithTransportCredentials(cred))
	}
	// 单向认证
	if !enableCa && enableTls {
		// 这里Common Name 为刚刚生成自签证书所填写的
		cred, err := credentials.NewClientTLSFromFile(tlsCa, "localhost")
		if err != nil {
			fmt.Printf("grpc client read cert and key file failed, err:%v\n", err)
			return nil, nil
		}
		return grpc.Dial(grpcAP, grpc.WithTransportCredentials(cred))
	}
	// 无认证
	return grpc.Dial(grpcAP, grpc.WithInsecure())
}
