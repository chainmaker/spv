/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package conf

import (
	"fmt"
	"strconv"

	"chainmaker.org/chainmaker/spv/v2/logger"
)

// LocalConfig is the config containing all chains' spv config
type LocalConfig struct {
	Chains        []*ChainConfig    `mapstructure:"chains"`
	GRPCConfig    *GRPCConfig       `mapstructure:"grpc"`
	WebConfig     *WebConfig        `mapstructure:"web"`
	StorageConfig *StoreConfig      `mapstructure:"storage"`
	LogConfig     *logger.LogConfig `mapstructure:"log"`
}

// GetChainConfig get chain config
func (c *LocalConfig) GetChainConfig(chainId string) (*ChainConfig, error) {
	for _, chain := range c.Chains {
		if chain.ChainId == chainId {
			return chain, nil
		}
	}
	return nil, fmt.Errorf("no exist this chainId:%s", chainId)
}

// ChainConfig is the config of one chain's spv config
type ChainConfig struct {
	ChainType      string             `mapstructure:"chain_type"`
	ChainId        string             `mapstructure:"chain_id"`
	SyncInterval   int32              `mapstructure:"sync_interval"`
	BlockInterval  int32              `mapstructure:"block_interval"`
	ConcurrentNums int32              `mapstructure:"concurrent_nums"`
	SDKConfigPath  string             `mapstructure:"sdk_config_path"`
	FabricConfig   *FabricExtraConfig `mapstructure:"fabric_extra_config"`
}

// FabricExtraConfig is the extra config of fabric, containing user and peers
type FabricExtraConfig struct {
	Peers []*Peer `mapstructure:"peers"`
}

// Peer contains the peer name of fabric
type Peer struct {
	PeerName string `mapstructure:"peer"`
}

// GRPCConfig is the config of gRPC module
type GRPCConfig struct {
	Address   string             `mapstructure:"address"`
	Port      int                `mapstructure:"port"`
	EnableTLS bool               `mapstructure:"enable_tls"`
	Security  *TransportSecurity `mapstructure:"security"`
}

// ToUrl return url of grpc config
func (gRPCConfig *GRPCConfig) ToUrl() string {
	return gRPCConfig.Address + ":" + strconv.Itoa(gRPCConfig.Port)
}

// WebConfig is the config of web module
type WebConfig struct {
	Address   string             `mapstructure:"address"`
	Port      int                `mapstructure:"port"`
	EnableTLS bool               `mapstructure:"enable_tls"`
	Security  *TransportSecurity `mapstructure:"security"`
}

// ToUrl return url of web config
func (webConfig *WebConfig) ToUrl() string {
	return webConfig.Address + ":" + strconv.Itoa(webConfig.Port)
}

// TransportSecurity 传输安全配置
type TransportSecurity struct {
	EnableCertAuth bool     `mapstructure:"ca_auth"`
	CAFile         []string `mapstructure:"ca_file"`
	CertFile       string   `mapstructure:"cert_file"`
	KeyFile        string   `mapstructure:"key_file"`
}

// StoreConfig is the config of storage module
type StoreConfig struct {
	Provider string         `mapstructure:"provider"`
	LevelDB  *LevelDBConfig `mapstructure:"leveldb"`
}

// LevelDBConfig is the config of leveldb module
type LevelDBConfig struct {
	StorePath       string `mapstructure:"store_path"`
	WriteBufferSize int    `mapstructure:"write_buffer_size"`
	BloomFilterBits int    `mapstructure:"bloom_filter_bits"`
}
