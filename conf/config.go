/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package conf contains the logic to read and initialize system configuration
package conf

import (
	"errors"
	"path/filepath"
	"strings"

	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

// spv type
const (
	ChainMakerSPV   = "chainmaker_spv"
	ChainMakerLight = "chainmaker_light"
	FabricSPV       = "fabric_spv"
)

var (
	flagSets = make([]*pflag.FlagSet, 0) // flag set
	// Filepath is default config file path
	Filepath = "../config/spv.yml"
	// SPVConfig global config containing all chains' spv config
	SPVConfig = &LocalConfig{}
	// RemoteChainConfigs all remote chains' config got by sdk
	RemoteChainConfigs = make(map[string]*protogo.RemoteConfig)
)

// InitSPVConfig inits SPVConfig when deployed independently
func InitSPVConfig(cmd *cobra.Command) error {
	// 1. init config
	config, err := initSPVConfig(cmd)
	if err != nil {
		return err
	}
	// 2. set chain type to lower and set sdk config path to abs path
	for i := 0; i < len(config.Chains); i++ {
		config.Chains[i].ChainType = strings.ToLower(config.Chains[i].ChainType)
		if !filepath.IsAbs(config.Chains[i].SDKConfigPath) {
			config.Chains[i].SDKConfigPath, _ = filepath.Abs(config.Chains[i].SDKConfigPath)
		}
	}
	// 3. set log config
	logger.SetLogConfig(config.LogConfig)
	// 4. set global config
	SPVConfig = config
	return nil
}

// InitSPVConfigWithYmlFile inits SPVConfig when integrated as a component
func InitSPVConfigWithYmlFile(ymlFile string) error {
	// 1. init spv config
	config, err := initSPVConfigWithYmlFile(ymlFile)
	if err != nil {
		return err
	}
	// 2. set chain type to lower and set sdk config path to abs path
	for i := 0; i < len(config.Chains); i++ {
		config.Chains[i].ChainType = strings.ToLower(config.Chains[i].ChainType)
		if !filepath.IsAbs(config.Chains[i].SDKConfigPath) {
			config.Chains[i].SDKConfigPath, _ = filepath.Abs(config.Chains[i].SDKConfigPath)
		}
	}
	// 3. set global config
	SPVConfig = config
	return nil
}

func initSPVConfig(cmd *cobra.Command) (*LocalConfig, error) {
	// 1. load env
	cmViper := viper.New()
	err := cmViper.BindPFlags(cmd.PersistentFlags())
	if err != nil {
		return nil, err
	}
	// 2. load the path of the config files
	ymlFile := Filepath
	if !filepath.IsAbs(ymlFile) {
		ymlFile, _ = filepath.Abs(ymlFile)
	}
	// 3. load the config file
	cmViper.SetConfigFile(ymlFile)
	if err = cmViper.ReadInConfig(); err != nil {
		return nil, err
	}
	for _, command := range cmd.Commands() {
		flagSets = append(flagSets, command.PersistentFlags())
		err = cmViper.BindPFlags(command.PersistentFlags())
		if err != nil {
			return nil, err
		}
	}
	// 4. new spv config instance
	config := &LocalConfig{}
	if err = cmViper.Unmarshal(config); err != nil {
		return nil, err
	}
	if err = checkYmlFile(config); err != nil {
		return nil, err
	}
	return config, nil
}

func initSPVConfigWithYmlFile(ymlFile string) (*LocalConfig, error) {
	cmViper := viper.New()
	// 1. load the path of the config files
	if ymlFile == "" {
		ymlFile = Filepath
	}
	if !filepath.IsAbs(ymlFile) {
		ymlFile, _ = filepath.Abs(ymlFile)
	}
	// 2. load the config file
	cmViper.SetConfigFile(ymlFile)
	if err := cmViper.ReadInConfig(); err != nil {
		return nil, err
	}
	// 3. new spv config instance
	config := &LocalConfig{}
	if err := cmViper.Unmarshal(config); err != nil {
		return nil, err
	}
	if err := checkYmlFile(config); err != nil {
		return nil, err
	}
	return config, nil
}

func checkYmlFile(config *LocalConfig) error {
	if len(config.Chains) == 0 {
		return errors.New("chains config should not be empty")
	}
	if config.GRPCConfig == nil {
		return errors.New("grpc config should not be nil")
	}
	if config.GRPCConfig.Security == nil {
		return errors.New("security in grpc config should not be nil")
	}
	if config.WebConfig == nil {
		return errors.New("web config should not be nil")
	}
	if config.WebConfig.Security == nil {
		return errors.New("security in web config should not be nil")
	}
	if config.StorageConfig == nil {
		return errors.New("store config should not be nil")
	}
	if config.LogConfig == nil {
		return errors.New("store config should not be nil")
	}
	return nil
}
