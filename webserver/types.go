/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package webserver

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

// state message
const (
	Ok                    string = "OK"                    // 正常结果码
	InvalidParameter      string = "InvalidParameter"      // 参数错误（包括参数格式、类型等错误）
	InvalidParameterValue string = "InvalidParameterValue" // 参数取值错误
	MissingParameter      string = "MissingParameter"      // 缺少参数错误，必传参数没填
	UnknownParameter      string = "UnknownParameter"      // 未知参数错误，用户多传未定义的参数会导致错误
	SignatureExpire       string = "SignatureExpire"       // 签名过期
	UnauthorizedOperation string = "UnauthorizedOperation" // 请求未CAM授权
	SecretIdNotFound      string = "SecretIdNotFound"      // 密钥不存在
	SignatureFailure      string = "SignatureFailure"      // 签名错误
	TokenFailure          string = "TokenFailure"          // token错误
	MFAFailure            string = "MFAFailure"            // MFA错误
	InvalidSecretId       string = "InvalidSecretId"       // 密钥非法（不是云API密钥类型）
	InternalError         string = "InternalError"         // 内部错误
	InvalidAction         string = "InvalidAction"         // 接口不存在
	RequestLimitExceeded  string = "RequestLimitExceeded"  // 请求的次数超过了频率限制
	NoSuchVersion         string = "NoSuchVersion"         // 接口版本不存在
	UnsupportedRegion     string = "UnsupportedRegion"     // 接口不支持所传地域
	UnsupportedOperation  string = "UnsupportedOperation"  // 操作不支持
	ResourceNotFound      string = "ResourceNotFound"      // 资源不存在
	LimitExceeded         string = "LimitExceeded"         // 超过配额限制
	ResourceUnavailable   string = "ResourceUnavailable"   // 资源不可用
	ResourceInsufficient  string = "ResourceInsufficient"  // 资源不足
	FailedOperation       string = "FailedOperation"       // 操作失败
	ResourceInUse         string = "ResourceInUse"         // 资源被占用
	DryRunOperation       string = "DryRunOperation"       // DryRun操作，代表请求将会是成功的，只是多传了DryRun参数
	ResourcesSoldOut      string = "ResourcesSoldOut"      // 资源售罄
	OperationDenied       string = "OperationDenied"       // 操作被拒绝
	DBOperateError        string = "DBOperateError"        // 数据库操作错误
	RecordNotFound        string = "RecordNotFound"        // 数据库中数据不存在错误
)

// Response struct
type Response struct {
	Code    string      `json:"code,omitempty"` // omit when success
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

// WithChainId struct
type WithChainId struct {
	ChainId    string `json:"chainId"`
	FromRemote bool   `json:"fromRemote"`
}

// IsComplete method
func (w *WithChainId) IsComplete() error {
	if len(w.ChainId) == 0 {
		return errors.New("chainId should not be empty")
	}
	return nil
}

// WithChainIdAndHeight struct
type WithChainIdAndHeight struct {
	ChainId    string `json:"chainId"`
	Height     uint64 `json:"height"`
	FromRemote bool   `json:"fromRemote"`
}

// IsComplete method
func (w *WithChainIdAndHeight) IsComplete() error {
	if len(w.ChainId) == 0 {
		return errors.New("chainId should not be empty")
	}
	return nil
}

// WithChainIdAndHash struct
type WithChainIdAndHash struct {
	ChainId    string `json:"chainId"`
	Hash       string `json:"hash"`
	FromRemote bool   `json:"fromRemote"`
}

// IsComplete method
func (w *WithChainIdAndHash) IsComplete() error {
	if len(w.ChainId) == 0 {
		return errors.New("chainId should not be empty")
	}
	if len(w.Hash) == 0 {
		return errors.New("hash should not be empty")
	}
	return nil
}

// WithChainIdAndTxKey struct
type WithChainIdAndTxKey struct {
	ChainId    string `json:"chainId"`
	TxKey      string `json:"txKey"`
	FromRemote bool   `json:"fromRemote"`
}

// IsComplete method
func (w *WithChainIdAndTxKey) IsComplete() error {
	if len(w.ChainId) == 0 {
		return errors.New("chainId should not be empty")
	}
	if len(w.TxKey) == 0 {
		return errors.New("txKey should not be empty")
	}
	return nil
}

// JsonOKResponse return ok json
func JsonOKResponse(ctx *gin.Context, data interface{}) {
	ctx.JSON(http.StatusOK, Response{
		Data: data,
	})
}

// JsonErrResponse return error json
func JsonErrResponse(ctx *gin.Context, code string, err error) {
	ctx.JSON(http.StatusOK, Response{
		Code:    code,
		Message: err.Error(),
	})
}
