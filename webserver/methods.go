/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package webserver

import (
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/adapter"
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common/chainmaker_common"
	"chainmaker.org/chainmaker/spv/v2/common/fabric_common"
	"chainmaker.org/chainmaker/spv/v2/pb/api"
	"chainmaker.org/chainmaker/spv/v2/server"
	"github.com/gin-gonic/gin"
	fabCommonPb "github.com/hyperledger/fabric-protos-go/common"
	"go.uber.org/zap"
)

var (
	spvServer *server.SPVServer
	log       *zap.SugaredLogger
)

// method name
const (
	ValidTransaction       = "ValidTransaction"
	GetBlockByHeight       = "GetBlockByHeight"
	GetBlockByHash         = "GetBlockByHash"
	GetBlockByTxKey        = "GetBlockByTxKey"
	GetLastBlock           = "GetLastBlock"
	GetTransactionByTxKey  = "GetTransactionByTxKey"
	GetTransactionTotalNum = "GetTransactionTotalNum"
	GetBlockTotalNum       = "GetBlockTotalNum"
	GetCurrentBlockHeight  = "GetCurrentBlockHeight"
)

// validTransaction verifies the validity of transaction
func validTransaction(ctx *gin.Context) {
	// 0.get param
	log.Infof("Receive http request[%s]", ctx.Request.URL.String())
	request := &api.TxValidationRequest{}
	if err := ctx.ShouldBindJSON(request); err != nil {
		JsonErrResponse(ctx, InvalidParameter, err)
		return
	}
	var response *api.TxValidationResponse
	err := spvServer.ValidTransaction(request)
	if err == nil {
		response = &api.TxValidationResponse{
			ChainId: request.ChainId,
			TxKey:   request.TxKey,
			Code:    api.Code_Valid,
			Message: "valid transaction",
		}
		JsonOKResponse(ctx, response)
	} else {
		response = &api.TxValidationResponse{
			ChainId: request.ChainId,
			TxKey:   request.TxKey,
			Code:    api.Code_Invalid,
			Message: err.Error(),
		}
		log.Warnf("WebServer verifies an invalid transaction, chainId:%s, txKey:%s, err:%v",
			request.ChainId, request.TxKey, err)
		JsonOKResponse(ctx, response)
	}
}

// getBlockByHeight returns block by height from remote chain or local spv to web client
func getBlockByHeight(ctx *gin.Context) {
	// 0.get param
	log.Infof("Receive http request[%s]", ctx.Request.URL.String())
	wc := &WithChainIdAndHeight{}
	if err := ctx.ShouldBindJSON(wc); err != nil {
		JsonErrResponse(ctx, InvalidParameter, err)
		return
	}
	if err := wc.IsComplete(); err != nil {
		JsonErrResponse(ctx, MissingParameter, err)
		return
	}
	// 1.get db and coder
	db := spvServer.GetDB()
	coder, err := spvServer.GetCoder(wc.ChainId)
	if err != nil {
		JsonErrResponse(ctx, InvalidParameterValue, err)
		return
	}
	var sdk adapter.SDKAdapter
	var blocker common.Blocker
	// 2. if fromRemote is true, then get block from remote chain by sdk
	if wc.FromRemote {
		sdk, err = spvServer.GetAdapter(wc.ChainId)
		if err != nil {
			JsonErrResponse(ctx, InternalError, err)
			return
		}
		blocker, err = sdk.GetBlockByHeight(wc.Height)
		if err != nil {
			JsonErrResponse(ctx, InternalError, err)
			return
		}
		writeBlockByBlocker(ctx, wc.ChainId, blocker)
		return
	}
	// 2.if fromRemote is false, get block from local without block body
	serialHeader, ok := db.GetBlockHeaderByHeight(wc.ChainId, wc.Height)
	if !ok {
		JsonErrResponse(ctx, RecordNotFound, fmt.Errorf("get block header in local by height failed, block height:%d",
			wc.Height))
		return
	}
	header, err := coder.DeserializeBlockHeader(serialHeader)
	if err != nil {
		JsonErrResponse(ctx, InternalError, err)
		return
	}
	writeBlockByHeader(ctx, wc.ChainId, header)
}

// getBlockByHash returns block by hash from remote chain or local spv to web client
func getBlockByHash(ctx *gin.Context) {
	// 0.get param
	log.Infof("Receive http request[%s]", ctx.Request.URL.String())
	wc := &WithChainIdAndHash{}
	if err := ctx.ShouldBindJSON(wc); err != nil {
		JsonErrResponse(ctx, InvalidParameter, err)
		return
	}
	if err := wc.IsComplete(); err != nil {
		JsonErrResponse(ctx, MissingParameter, err)
		return
	}
	// 1.get db and coder
	db := spvServer.GetDB()
	coder, err := spvServer.GetCoder(wc.ChainId)
	if err != nil {
		JsonErrResponse(ctx, InvalidParameterValue, err)
		return
	}
	// get header and height by block hash
	var hashBz []byte
	if hashBz, err = hex.DecodeString(wc.Hash); err != nil {
		if hashBz, err = base64.StdEncoding.DecodeString(wc.Hash); err != nil {
			JsonErrResponse(ctx, InvalidParameterValue,
				errors.New("invalid block hash, block hash must be base64 or hex encoding"))
			return
		}
	}
	if err != nil {
		JsonErrResponse(ctx, InvalidParameterValue, err)
		return
	}
	headerBz, h, ok := db.GetBlockHeaderAndHeightByHash(wc.ChainId, hashBz)
	if !ok {
		JsonErrResponse(ctx, RecordNotFound, fmt.Errorf("get block header in local by hash failed, block hash:%x", hashBz))
		return
	}
	var sdk adapter.SDKAdapter
	var blocker common.Blocker
	// 2. if fromRemote is true, then get block from remote chain by sdk
	if wc.FromRemote {
		sdk, err = spvServer.GetAdapter(wc.ChainId)
		if err != nil {
			JsonErrResponse(ctx, InternalError, err)
			return
		}
		blocker, err = sdk.GetBlockByHeight(h)
		if err != nil {
			JsonErrResponse(ctx, InternalError, err)
			return
		}
		writeBlockByBlocker(ctx, wc.ChainId, blocker)
		return
	}
	// 2.if fromRemote is false, get block from local without block body
	header, err := coder.DeserializeBlockHeader(headerBz)
	if err != nil {
		JsonErrResponse(ctx, InternalError, err)
		return
	}
	writeBlockByHeader(ctx, wc.ChainId, header)
}

// getBlockByTxKey returns block by txId from remote chain or local spv to web client
func getBlockByTxKey(ctx *gin.Context) {
	// 0.get param
	log.Infof("Receive http request[%s]", ctx.Request.URL.String())
	wc := &WithChainIdAndTxKey{}
	if err := ctx.ShouldBindJSON(wc); err != nil {
		JsonErrResponse(ctx, InvalidParameter, err)
		return
	}
	if err := wc.IsComplete(); err != nil {
		JsonErrResponse(ctx, MissingParameter, err)
		return
	}
	// 1.get db and coder
	db := spvServer.GetDB()
	coder, err := spvServer.GetCoder(wc.ChainId)
	if err != nil {
		JsonErrResponse(ctx, InvalidParameterValue, err)
		return
	}
	// get block height of the tx
	_, h, ok := db.GetTransactionHashAndHeightByTxKey(wc.ChainId, wc.TxKey)
	if !ok {
		JsonErrResponse(ctx, RecordNotFound, fmt.Errorf("get block height in local by txKey failed, txKey:%s", wc.TxKey))
		return
	}
	var sdk adapter.SDKAdapter
	var blocker common.Blocker
	// 2. if fromRemote is true, then get block from remote chain by sdk
	if wc.FromRemote {
		sdk, err = spvServer.GetAdapter(wc.ChainId)
		if err != nil {
			JsonErrResponse(ctx, InternalError, err)
			return
		}
		blocker, err = sdk.GetBlockByHeight(h)
		if err != nil {
			JsonErrResponse(ctx, InternalError, err)
			return
		}
		writeBlockByBlocker(ctx, wc.ChainId, blocker)
		return
	}
	// 2.if fromRemote is false, get block from local without block body
	headerBz, ok := db.GetBlockHeaderByHeight(wc.ChainId, h)
	if !ok {
		JsonErrResponse(ctx, RecordNotFound, fmt.Errorf("get block header in local by height failed, block height:%d", h))
		return
	}
	header, err := coder.DeserializeBlockHeader(headerBz)
	if err != nil {
		JsonErrResponse(ctx, InternalError, err)
		return
	}
	writeBlockByHeader(ctx, wc.ChainId, header)
}

// getLastBlock returns last committed block from remote chain or local spv to web client
func getLastBlock(ctx *gin.Context) {
	// 0.get param
	log.Infof("Receive http request[%s]", ctx.Request.URL.String())
	wc := &WithChainId{}
	if err := ctx.ShouldBindJSON(wc); err != nil {
		JsonErrResponse(ctx, InvalidParameter, err)
		return
	}
	if err := wc.IsComplete(); err != nil {
		JsonErrResponse(ctx, MissingParameter, err)
		return
	}
	// 1.get db and coder
	db := spvServer.GetDB()
	coder, err := spvServer.GetCoder(wc.ChainId)
	if err != nil {
		JsonErrResponse(ctx, InvalidParameterValue, err)
		return
	}
	// get the height of last committed block
	headerBz, h, ok := db.GetLastCommittedBlockHeaderAndHeight(wc.ChainId)
	if !ok {
		JsonErrResponse(ctx, RecordNotFound, fmt.Errorf("get last committed block header failed, chainId:%s", wc.ChainId))
		return
	}
	var sdk adapter.SDKAdapter
	var blocker common.Blocker
	// 2. if fromRemote is true, then get last committed block from remote chain by sdk
	if wc.FromRemote {
		sdk, err = spvServer.GetAdapter(wc.ChainId)
		if err != nil {
			JsonErrResponse(ctx, InternalError, err)
			return
		}
		blocker, err = sdk.GetBlockByHeight(h)
		if err != nil {
			JsonErrResponse(ctx, InternalError, err)
			return
		}
		writeBlockByBlocker(ctx, wc.ChainId, blocker)
		return
	}
	// 2.if fromRemote is false, get last committed block from local without block body
	header, err := coder.DeserializeBlockHeader(headerBz)
	if err != nil {
		JsonErrResponse(ctx, InternalError, err)
		return
	}
	writeBlockByHeader(ctx, wc.ChainId, header)
}

// getTxByTxKey returns the transaction by txId from remote chain or local spv to web client
func getTxByTxKey(ctx *gin.Context) {
	// 0.get param
	log.Infof("Receive http request[%s]", ctx.Request.URL.String())
	wc := &WithChainIdAndTxKey{}
	if err := ctx.ShouldBindJSON(wc); err != nil {
		JsonErrResponse(ctx, InvalidParameter, err)
		return
	}
	if err := wc.IsComplete(); err != nil {
		JsonErrResponse(ctx, MissingParameter, err)
		return
	}
	// 1.get db and coder
	db := spvServer.GetDB()
	coder, err := spvServer.GetCoder(wc.ChainId)
	if err != nil {
		JsonErrResponse(ctx, InvalidParameterValue, err)
		return
	}
	var sdk adapter.SDKAdapter
	var txer common.Transactioner
	// 2.get transaction from remote chain by sdk
	if wc.FromRemote {
		sdk, err = spvServer.GetAdapter(wc.ChainId)
		if err != nil {
			JsonErrResponse(ctx, InternalError, err)
			return
		}
		txer, err = sdk.GetTransactionByTxKey(wc.TxKey)
		if err != nil {
			JsonErrResponse(ctx, InternalError, err)
			return
		}
		writeTransaction(ctx, wc.ChainId, txer)
		return
	}
	// 2.get transaction from local
	txBz, ok := db.GetTransactionByTxKey(wc.ChainId, wc.TxKey)
	if !ok {
		JsonErrResponse(ctx, RecordNotFound, fmt.Errorf("get transaction in local by txKey failed, txKey:%s", wc.TxKey))
		return
	}
	rwBz, _ := db.GetTxExtraDataByTxKey(wc.ChainId, wc.TxKey)
	txer, err = coder.DeserializeTransaction(txBz, rwBz)
	if err != nil {
		JsonErrResponse(ctx, InternalError, err)
		return
	}
	writeTransaction(ctx, wc.ChainId, txer)
}

// getTxNums returns the total number of transactions of spv to web client
func getTxNums(ctx *gin.Context) {
	// 0.get param
	log.Infof("Receive http request[%s]", ctx.Request.URL.String())
	wc := &WithChainId{}
	if err := ctx.ShouldBindJSON(wc); err != nil {
		JsonErrResponse(ctx, InvalidParameter, err)
		return
	}
	if err := wc.IsComplete(); err != nil {
		JsonErrResponse(ctx, MissingParameter, err)
		return
	}
	// 1.get tx nums
	db := spvServer.GetDB()
	totalNum, ok := db.GetTransactionTotalNum(wc.ChainId)
	if !ok {
		JsonErrResponse(ctx, RecordNotFound, fmt.Errorf("get the total num of transactions failed, chainId:%s", wc.ChainId))
		return
	}
	JsonOKResponse(ctx, totalNum)
}

// getBlockNums returns the total number of blocks of spv to web client
func getBlockNums(ctx *gin.Context) {
	// 0.get param
	log.Infof("Receive http request[%s]", ctx.Request.URL.String())
	wc := &WithChainId{}
	if err := ctx.ShouldBindJSON(wc); err != nil {
		JsonErrResponse(ctx, InvalidParameter, err)
		return
	}
	if err := wc.IsComplete(); err != nil {
		JsonErrResponse(ctx, MissingParameter, err)
		return
	}
	// 1.get block nums
	db := spvServer.GetDB()
	_, h, ok := db.GetLastCommittedBlockHeaderAndHeight(wc.ChainId)
	if !ok {
		JsonErrResponse(ctx, RecordNotFound, fmt.Errorf("get the total num of blocks failed, chainId:%s", wc.ChainId))
		return
	}
	JsonOKResponse(ctx, h+1)
}

// getCurrentHeight returns the current block height of spv to web client
func getCurrentHeight(ctx *gin.Context) {
	// 0.get param
	log.Infof("Receive http request[%s]", ctx.Request.URL.String())
	wc := &WithChainId{}
	if err := ctx.ShouldBindJSON(wc); err != nil {
		JsonErrResponse(ctx, InvalidParameter, err)
		return
	}
	if err := wc.IsComplete(); err != nil {
		JsonErrResponse(ctx, MissingParameter, err)
		return
	}
	// 1.get block num
	db := spvServer.GetDB()
	_, h, ok := db.GetLastCommittedBlockHeaderAndHeight(wc.ChainId)
	if !ok {
		JsonErrResponse(ctx, RecordNotFound, fmt.Errorf("get the total num of blocks failed, chainId:%s", wc.ChainId))
		return
	}
	JsonOKResponse(ctx, h)
}

func writeBlockByBlocker(ctx *gin.Context, chainId string, blocker common.Blocker) {
	switch block := blocker.(type) {
	case *chainmaker_common.CMBlock:
		JsonOKResponse(ctx, block.Block)
		return
	case *fabric_common.FabBlock:
		JsonOKResponse(ctx, block.Block)
		return
	default:
		JsonErrResponse(ctx, UnsupportedOperation, errors.New("unsupport chain type"))
	}
}

func writeBlockByHeader(ctx *gin.Context, chainId string, header common.Header) {
	switch head := header.(type) {
	case *chainmaker_common.CMBlockHeader:
		cmb := &cmCommonPb.Block{Header: head.BlockHeader}
		JsonOKResponse(ctx, cmb)
		return
	case *fabric_common.FabBlockHeader:
		fab := &fabCommonPb.Block{Header: head.BlockHeader}
		JsonOKResponse(ctx, fab)
		return
	default:
		JsonErrResponse(ctx, UnsupportedOperation, errors.New("unsupport chain type"))
	}
}

func writeTransaction(ctx *gin.Context, chainId string, txer common.Transactioner) {
	switch tx := txer.(type) {
	case *chainmaker_common.CMTransaction:
		JsonOKResponse(ctx, tx)
		return
	case *fabric_common.FabTransaction:
		JsonOKResponse(ctx, tx.Transaction)
		return
	default:
		JsonErrResponse(ctx, UnsupportedOperation, errors.New("unsupport chain type"))
	}
}
