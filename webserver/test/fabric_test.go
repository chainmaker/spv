/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package test

//import (
//	"encoding/json"
//	"testing"
//
//	"chainmaker.org/chainmaker/spv/v2/pb/api"
//	"chainmaker.org/chainmaker/spv/v2/webserver"
//	"github.com/stretchr/testify/require"
//)
//
//const (
//	fabChainId = "mychannel"
//)
//
//func Test_fab_validTransaction(t *testing.T) {
//	request := &api.TxValidationRequest{
//		ChainId:     fabChainId,
//		BlockHeight: 6,
//		Index:       -1,
//		TxKey:       "d14134d322dfaeac1ed1e6b1ee9c5027a2cd05b369a0b02aad6985645a339215",
//		ContractData: &api.ContractData{
//			Name:   "mycc",
//			Method: "invoke",
//			Params: []*api.KVPair{
//				{
//					Key:   "",
//					Value: []byte("a"),
//				},
//				{
//					Key:   "",
//					Value: []byte("b"),
//				},
//				{
//					Key:   "",
//					Value: []byte("10"),
//				},
//			},
//		},
//		Timeout: 5000,
//		Extra:   nil,
//	}
//	bz, err := json.Marshal(request)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/ValidTransaction", bz)
//	require.Nil(t, err)
//}
//
//func Test_fab_getBlockByHeight(t *testing.T) {
//	wc := &webserver.WithChainIdAndHeight{
//		ChainId:    fabChainId,
//		Height:     1,
//		FromRemote: true,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetBlockByHeight", bz)
//	require.Nil(t, err)
//}
//
//func Test_fab_getBlockByHash(t *testing.T) {
//	wc := &webserver.WithChainIdAndHash{
//		ChainId:    fabChainId,
//		Hash:       "2690b33474e32970270e88d390cc8681cf5c8ac26d5696fcf9379c261087106a",
//		FromRemote: false,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetBlockByHash", bz)
//	require.Nil(t, err)
//}
//
//func Test_fab_getBlockByTxKey(t *testing.T) {
//	wc := &webserver.WithChainIdAndTxKey{
//		ChainId:    fabChainId,
//		TxKey:      "d14134d322dfaeac1ed1e6b1ee9c5027a2cd05b369a0b02aad6985645a339215",
//		FromRemote: true,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetBlockByTxKey", bz)
//	require.Nil(t, err)
//}
//
//func Test_fab_getLastBlock(t *testing.T) {
//	wc := &webserver.WithChainId{
//		ChainId:    fabChainId,
//		FromRemote: false,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetLastBlock", bz)
//	require.Nil(t, err)
//}
//
//func Test_fab_getTransactionByTxKey(t *testing.T) {
//	wc := &webserver.WithChainIdAndTxKey{
//		ChainId:    fabChainId,
//		TxKey:      "d14134d322dfaeac1ed1e6b1ee9c5027a2cd05b369a0b02aad6985645a339215",
//		FromRemote: false,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetTransactionByTxKey", bz)
//	require.Nil(t, err)
//}
//
//func Test_fab_getTransactionTotalNum(t *testing.T) {
//	wc := &webserver.WithChainId{
//		ChainId: fabChainId,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetTransactionTotalNum", bz)
//	require.Nil(t, err)
//}
//
//func Test_fab_getBlockTotalNum(t *testing.T) {
//	wc := &webserver.WithChainId{
//		ChainId: fabChainId,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetBlockTotalNum", bz)
//	require.Nil(t, err)
//}
//
//func Test_fab_getCurrentBlockHeight(t *testing.T) {
//	wc := &webserver.WithChainId{
//		ChainId: fabChainId,
//	}
//	bz, err := json.Marshal(wc)
//	require.Nil(t, err)
//	err = sendPostRequest("http://localhost:12346/GetCurrentBlockHeight", bz)
//	require.Nil(t, err)
//}
