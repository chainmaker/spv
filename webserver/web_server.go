/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package webserver

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"net/http"
	"os"
	"time"

	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/server"
	"github.com/gin-gonic/gin"
)

// WebServer is a web listener of web for SPV and light node
type WebServer struct {
	server *http.Server
}

// NewWebServer creates a instance of web server
func NewWebServer(spvSer *server.SPVServer) *WebServer {
	// set gin mode
	gin.SetMode(gin.ReleaseMode)
	// generate route
	ginRouter := gin.Default()
	// init router
	initRouter(ginRouter)
	// new server
	webSrv := &http.Server{
		Addr:    conf.SPVConfig.WebConfig.ToUrl(),
		Handler: ginRouter,
	}
	// init spvServer and log
	spvServer = spvSer
	log = logger.GetLogger(logger.ModuleWeb)
	return &WebServer{
		server: webSrv,
	}
}

// Start starts up web server
func (ws *WebServer) Start() error {
	// enable tls
	if conf.SPVConfig.WebConfig.EnableTLS {
		security := conf.SPVConfig.WebConfig.Security
		// enable ca auth
		if security.EnableCertAuth {
			pool := x509.NewCertPool()
			for _, caFile := range security.CAFile {
				caCertBz, err := os.ReadFile(caFile)
				if err != nil {
					log.Errorf("web server read ca file failed,err:%v", err)
				}
				pool.AppendCertsFromPEM(caCertBz)
			}
			ws.server.TLSConfig = &tls.Config{ // nolint
				ClientCAs:  pool,
				ClientAuth: tls.RequireAndVerifyClientCert,
			}
		}
		go func() {
			err := ws.server.ListenAndServeTLS(security.CertFile, security.KeyFile)
			if err != nil && err != http.ErrServerClosed {
				log.Errorf("Web Server TLS Start Failed! err:%v", err)
			}
		}()
		log.Infof("Web Server TLS Listen on HTTPS %s", conf.SPVConfig.WebConfig.ToUrl())
		return nil
	}
	// not enable tls
	go func() {
		// service connections
		if err := ws.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Errorf("Web Server Start Failed! err:%v", err)
		}
	}()
	log.Infof("Web Server Listen on HTTP %s", conf.SPVConfig.WebConfig.ToUrl())
	return nil
}

// Stop stops web server
func (ws *WebServer) Stop() error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := ws.server.Shutdown(ctx); err != nil {
		log.Errorf("Web Server Stopped Failed! err:%s", err)
	}
	log.Info("Stop Web Server!")
	return nil
}

func initRouter(router *gin.Engine) {
	group := router.Group("/")
	group.POST(ValidTransaction, validTransaction)
	group.POST(GetBlockByHeight, getBlockByHeight)
	group.POST(GetBlockByHash, getBlockByHash)
	group.POST(GetBlockByTxKey, getBlockByTxKey)
	group.POST(GetLastBlock, getLastBlock)
	group.POST(GetTransactionByTxKey, getTxByTxKey)
	group.POST(GetTransactionTotalNum, getTxNums)
	group.POST(GetBlockTotalNum, getBlockNums)
	group.POST(GetCurrentBlockHeight, getCurrentHeight)
}
