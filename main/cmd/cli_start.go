/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package cmd

import (
	"os"
	"os/signal"
	"syscall"

	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/rpcserver"
	"chainmaker.org/chainmaker/spv/v2/server"
	"chainmaker.org/chainmaker/spv/v2/webserver"
	"github.com/spf13/cobra"
	"go.uber.org/zap"
)

var log *zap.SugaredLogger

// StartCMD start cmd
func StartCMD() *cobra.Command {
	startCmd := &cobra.Command{
		Use:   "start",
		Short: "Startup SPV Server",
		Long:  "Startup ChainMaker SPV Server",
		RunE: func(cmd *cobra.Command, _ []string) error {
			initSpvConfig(cmd)
			start()
			return nil
		},
	}
	attachFlags(startCmd, []string{flagNameOfConfigFilepath})
	return startCmd
}

func start() {
	// 1. init spv server
	log = logger.GetLogger(logger.ModuleCli)
	spvServer, err := server.IntSPVServer()
	if err != nil {
		log.Errorf("Init SPV Server Failed! err:%v", err)
		return
	}
	if err = spvServer.Start(); err != nil {
		log.Errorf("Start SPV Server Failed! err:%v", err)
		return
	}
	// 2. init grpc server
	rpcServer, err := rpcserver.NewRPCServer(spvServer)
	if err != nil {
		log.Errorf("New GRPC Server Failed! err:%v", err)
	}
	if err = rpcServer.Start(); err != nil {
		log.Errorf("Startup GRPC Server Failed! err:%v", err)
	}
	// 3. init web server
	webServer := webserver.NewWebServer(spvServer)
	if err = webServer.Start(); err != nil {
		log.Errorf("Startup Web Server Failed! err:%v", err)
	}
	// 4. process exit signal
	errorC := make(chan error, 1)
	go handleExitSignal(errorC)
	// nolint
	select {
	case <-errorC:
		rpcServer.Stop()
		webServer.Stop()
		if err = spvServer.Stop(); err != nil {
			log.Errorf("Stop SPV Server Failed! err:%v", err)
		}
	}
}

func handleExitSignal(exitC chan<- error) {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGTERM, os.Interrupt, syscall.SIGINT)
	defer signal.Stop(signalChan)
	for sig := range signalChan {
		log.Infof("received signal:%d (%s)", sig, sig)
		exitC <- nil
	}
}
