/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package cmd

import (
	"fmt"
	"os"

	"chainmaker.org/chainmaker/spv/v2/conf"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

const (
	// command line options
	flagNameOfConfigFilepath          = "conf"
	flagNameShortHandOFConfigFilepath = "c"
)

func initSpvConfig(cmd *cobra.Command) {
	if err := conf.InitSPVConfig(cmd); err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
}

func initFlagSet() *pflag.FlagSet {
	flags := &pflag.FlagSet{}
	flags.StringVarP(&conf.Filepath, flagNameOfConfigFilepath, flagNameShortHandOFConfigFilepath, conf.Filepath,
		"specify config file path, if not set, default use ../config/spv.yml")
	return flags
}

func attachFlags(cmd *cobra.Command, flagNames []string) {
	flags := initFlagSet()
	cmdFlags := cmd.Flags()
	for _, flagName := range flagNames {
		if flag := flags.Lookup(flagName); flag != nil {
			cmdFlags.AddFlag(flag)
		}
	}
}
