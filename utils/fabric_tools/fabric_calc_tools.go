/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package fabric_tools defines tools for Fabric spv
package fabric_tools

import (
	"crypto/sha256"
	"encoding/asn1"
	"errors"
	"fmt"
	"math/big"

	"github.com/golang/protobuf/proto" // nolint
	fabCommonPb "github.com/hyperledger/fabric-protos-go/common"
	fabPeerPb "github.com/hyperledger/fabric-protos-go/peer"
)

// GetEnvelopeByBytes gets an envelope from a block's Data field.
func GetEnvelopeByBytes(data []byte) *fabCommonPb.Envelope {
	env := &fabCommonPb.Envelope{}
	if err := proto.Unmarshal(data, env); err != nil {
		panic("unmarshal fabric Envelope failed")
	}
	return env
}

// GetTransactionChannelId parses transaction for fabric to get channelId
func GetTransactionChannelId(envelope *fabCommonPb.Envelope) (string, error) {
	if envelope == nil {
		return "", errors.New("envelope should not be nil")
	}
	// Envelope -> Payload
	payload := &fabCommonPb.Payload{}
	err := proto.Unmarshal(envelope.Payload, payload)
	if err != nil {
		return "", err
	}
	// Payload.Header.ChannelHeader -> ChannelHeader
	channelHeader := &fabCommonPb.ChannelHeader{}
	err = proto.Unmarshal(payload.Header.ChannelHeader, channelHeader)
	if err != nil {
		return "", err
	}
	return channelHeader.ChannelId, nil
}

// GetTransactionId parses transaction for fabric to get transaction id
func GetTransactionId(envelope *fabCommonPb.Envelope) (string, error) {
	if envelope == nil {
		return "", errors.New("envelope should not be nil")
	}
	// Envelope -> Payload
	payload := &fabCommonPb.Payload{}
	err := proto.Unmarshal(envelope.Payload, payload)
	if err != nil {
		return "", err
	}
	// Payload.Header.ChannelHeader -> ChannelHeader
	channelHeader := &fabCommonPb.ChannelHeader{}
	err = proto.Unmarshal(payload.Header.ChannelHeader, channelHeader)
	if err != nil {
		return "", err
	}
	return channelHeader.TxId, nil
}

// GetTransactionContractData parses transaction for fabric to get contract name and args
func GetTransactionContractData(envelope *fabCommonPb.Envelope) (string, [][]byte, error) {
	var (
		contractName string
		args         [][]byte
	)
	if envelope == nil {
		return "", nil, errors.New("envelope should not be nil")
	}
	// Envelope -> Payload
	payload := &fabCommonPb.Payload{}
	err := proto.Unmarshal(envelope.Payload, payload)
	if err != nil {
		return "", nil, err
	}
	// Payload.Data -> Transaction
	transaction := &fabPeerPb.Transaction{}
	err = proto.Unmarshal(payload.Data, transaction)
	if err != nil {
		return "", nil, err
	}
	// TransactionAction.Payload -> ChaincodeActionPayload
	chaincodeActionPayload := &fabPeerPb.ChaincodeActionPayload{}
	err = proto.Unmarshal(transaction.Actions[0].Payload, chaincodeActionPayload)
	if err != nil {
		return "", nil, err
	}
	// ChaincodeActionPayload.ChaincodeProposalPayload -> ChaincodeProposalPayload
	chaincodeProposalPayload := &fabPeerPb.ChaincodeProposalPayload{}
	err = proto.Unmarshal(chaincodeActionPayload.ChaincodeProposalPayload, chaincodeProposalPayload)
	if err != nil {
		return "", nil, err
	}
	// ChaincodeProposalPayload.Input -> ChaincodeInvocationSpec
	chaincodeInvocationSpec := &fabPeerPb.ChaincodeInvocationSpec{}
	err = proto.Unmarshal(chaincodeProposalPayload.Input, chaincodeInvocationSpec)
	if err != nil {
		return "", nil, err
	}
	// ChaincodeInvocationSpec -> ChaincodeSpec
	chaincodeSpec := chaincodeInvocationSpec.ChaincodeSpec
	// get contract name and args
	contractName = chaincodeSpec.ChaincodeId.Name
	args = chaincodeSpec.Input.Args
	return contractName, args, nil
}

// GetValidationCodeByMetadata gets tx validation code in the block metadata
func GetValidationCodeByMetadata(index int, meta *fabCommonPb.BlockMetadata) int32 {
	if len(meta.Metadata) < 3 {
		return -1
	}
	// get the slice of tx validation code in the block
	vcSlice := meta.Metadata[2] // BlockMetadataIndex_TRANSACTIONS_FILTER is 2 in fabric
	if index < 0 || index >= len(vcSlice) {
		return -1
	}
	vc := vcSlice[index]
	return int32(vc)
}

// calc block hash for fabric
type asn1Header struct {
	Number       *big.Int
	PreviousHash []byte
	DataHash     []byte
}

// GetBlockHash calc block hash for fabric
func GetBlockHash(header *fabCommonPb.BlockHeader) []byte {
	h := asn1Header{
		PreviousHash: header.PreviousHash,
		DataHash:     header.DataHash,
		Number:       new(big.Int).SetUint64(header.Number),
	}
	data, err := asn1.Marshal(h)
	if err != nil {
		// Errors should only arise for types which cannot be encoded, since the
		// BlockHeader type is known a-priori to contain only encodable types, an
		// error here is fatal and should not be propagated
		panic(err)
	}
	sum := sha256.Sum256(data)
	return sum[:]
}

// GetBlockDataHash calc data hash by block data
func GetBlockDataHash(blockData *fabCommonPb.BlockData) []byte {
	dataBytes := concatenateBytes(blockData.Data...)
	sum := sha256.Sum256(dataBytes)
	return sum[:]
}

// concatenateBytes is useful for combining multiple arrays of bytes
func concatenateBytes(data ...[]byte) []byte {
	finalLength := 0
	for _, slice := range data {
		finalLength += len(slice)
	}
	result := make([]byte, finalLength)
	last := 0
	for _, slice := range data {
		for i := range slice {
			result[i+last] = slice[i]
		}
		last += len(slice)
	}
	return result
}

// NewBlock and NewEnvelope is test tools
func NewBlock() *fabCommonPb.Block {
	env := NewEnvelope()
	envBytes, err := proto.Marshal(env)
	if err != nil {
		fmt.Println(err)
	}
	dataHash := sha256.Sum256(envBytes)
	meta := make([]uint8, 1)
	meta[0] = uint8(0)

	block := &fabCommonPb.Block{
		Header: &fabCommonPb.BlockHeader{
			Number:       1,
			PreviousHash: []byte("preHash"),
			DataHash:     dataHash[:],
		},
		Data: &fabCommonPb.BlockData{
			Data: [][]byte{envBytes},
		},
		Metadata: &fabCommonPb.BlockMetadata{
			Metadata: [][]byte{
				nil,
				nil,
				meta,
			},
		},
	}
	return block
}

func NewEnvelope() *fabCommonPb.Envelope {
	chaincodeInvocationSpec := &fabPeerPb.ChaincodeInvocationSpec{
		ChaincodeSpec: &fabPeerPb.ChaincodeSpec{
			ChaincodeId: &fabPeerPb.ChaincodeID{
				Name: "myncc",
			},
			Input: &fabPeerPb.ChaincodeInput{
				Args: [][]byte{
					[]byte("invoke"),
					[]byte("a"),
					[]byte("b"),
					[]byte("10"),
				},
			},
		},
	}

	// ChaincodeProposalPayload.Input
	input, err := proto.Marshal(chaincodeInvocationSpec)
	if err != nil {
		fmt.Println(err)
	}
	chaincodeProposalPayload := &fabPeerPb.ChaincodeProposalPayload{
		Input: input,
	}
	// ChaincodeActionPayload.ChaincodeProposalPayload
	ccpPayload, err := proto.Marshal(chaincodeProposalPayload)
	if err != nil {
		fmt.Println(err)
	}
	chaincodeActionPayload := &fabPeerPb.ChaincodeActionPayload{
		ChaincodeProposalPayload: ccpPayload,
	}
	// TransactionAction.Payload
	actionPayload, err := proto.Marshal(chaincodeActionPayload)
	if err != nil {
		fmt.Println(err)
	}
	transaction := &fabPeerPb.Transaction{
		Actions: []*fabPeerPb.TransactionAction{
			{
				Payload: actionPayload,
			},
		},
	}
	// Payload.Header and Data
	channelHeader := &fabCommonPb.ChannelHeader{
		ChannelId: "mychannel",
		TxId:      "txId",
	}
	header, err := proto.Marshal(channelHeader)
	if err != nil {
		fmt.Println(err)
	}
	data, err := proto.Marshal(transaction)
	if err != nil {
		fmt.Println(err)
	}
	payload := &fabCommonPb.Payload{
		Header: &fabCommonPb.Header{
			ChannelHeader: header,
		},
		Data: data,
	}
	// Envelope.Payload
	payloadInEv, err := proto.Marshal(payload)
	if err != nil {
		fmt.Println(err)
	}
	env := &fabCommonPb.Envelope{
		Payload: payloadInEv,
	}
	return env
}
