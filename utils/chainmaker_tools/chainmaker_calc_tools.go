/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package chainmaker_tools defines tools for ChainMaker spv
package chainmaker_tools

import (
	"errors"
	"fmt"

	"chainmaker.org/chainmaker/common/v2/crypto/hash"
	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo/cm_pbgo"
	"github.com/gogo/protobuf/proto"
)

// CalcDagHash calculate DAG hash
func CalcDagHash(hashType string, dag *cmCommonPb.DAG) ([]byte, error) {
	if dag == nil {
		return nil, errors.New("calc hash block == nil")
	}
	dagBytes, err := proto.Marshal(dag)
	if err != nil {
		return nil, fmt.Errorf("marshal DAG error, %s", err)
	}
	hashByte, err := hash.GetByStrType(hashType, dagBytes)
	if err != nil {
		return nil, err
	}
	return hashByte, nil
}

// CalcRWSetRoot calculate txs' read-write set root hash, following the tx order in txs
func CalcRWSetRoot(hashType string, txs []*cmCommonPb.Transaction) ([]byte, error) {
	// calculate read-write set hash following the order in txs
	// if txId does not exist in txRWSetMap, fill in a default one
	if len(txs) == 0 {
		return nil, errors.New("calc rwset root set == nil")
	}
	rwSetHashes := make([][]byte, len(txs))
	for i, tx := range txs {
		rwSetHashes[i] = tx.Result.RwSetHash
	}
	// calculate the merkle root
	return hash.GetMerkleRoot(hashType, rwSetHashes)
}

// CalcTxHashWithVersion use version to judge if set tx gasUsed to zero when calc tx hash
func CalcTxHashWithVersion(t *cmCommonPb.Transaction, version int) ([]byte, error) {
	if version >= 2300 {
		if t.Result != nil && t.Result.ContractResult != nil {
			gasUsed := t.Result.ContractResult.GasUsed
			defer func() {
				t.Result.ContractResult.GasUsed = gasUsed
			}()

			t.Result.ContractResult.GasUsed = 0
		}
		return CalcTxHash(t)
	}

	return CalcTxHash(t)
}

// CalcTxHash calculate transaction hash
func CalcTxHash(transaction *cmCommonPb.Transaction) ([]byte, error) {
	if transaction == nil {
		return nil, errors.New("transaction is nil")
	}
	txBytes, err := transaction.Marshal()
	if err != nil {
		return nil, err
	}
	return hash.GetByStrType(conf.RemoteChainConfigs[transaction.Payload.ChainId].HashType, txBytes)
}

// ContractInfo contains contract data in the block
type ContractInfo struct {
	ContractName string
	Method       string
	Parameters   []interface{}
}

// NewContractInfo creates ContractInfo by contract name、contract method and contract params
func NewContractInfo(name, method string, params []*cmCommonPb.KeyValuePair) *ContractInfo {
	ci := &ContractInfo{
		ContractName: name,
		Method:       method,
	}

	l := len(params)
	parameters := make([]interface{}, l)
	for i := 0; i < l; i++ {
		parameters[i] = params[i]
	}
	ci.Parameters = parameters
	return ci
}

// GetContractInfoByTxPayload parses RequestPayload in the Transaction to generate ContractInfo
// which contains contract name、method and params
func GetContractInfoByTxPayload(transaction *cmCommonPb.Transaction) (*ContractInfo, error) {
	if transaction == nil {
		return nil, errors.New("transaction is nil")
	}
	if transaction.Payload == nil {
		return nil, errors.New("the payload in transaction is nil")
	}
	py := transaction.Payload
	return NewContractInfo(py.ContractName, py.Method, py.Parameters), nil
}

// TxRequestConverter converts *cm_pbgo.TxRequest to *cmCommonPb.TxRequest
func TxRequestConverter(request *cm_pbgo.TxRequest) (*cmCommonPb.TxRequest, error) {
	requestBytes, err := proto.Marshal(request)
	if err != nil {
		return nil, err
	}
	req := &cmCommonPb.TxRequest{}
	err = proto.Unmarshal(requestBytes, req)
	if err != nil {
		return nil, err
	}
	return req, nil
}

// TxResponseConverter converts *cmCommonPb.TxResponse to *cm_pbgo.TxResponse
func TxResponseConverter(response *cmCommonPb.TxResponse) (*cm_pbgo.TxResponse, error) {
	responseBytes, err := proto.Marshal(response)
	if err != nil {
		return nil, err
	}
	resp := &cm_pbgo.TxResponse{}
	err = proto.Unmarshal(responseBytes, resp)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
