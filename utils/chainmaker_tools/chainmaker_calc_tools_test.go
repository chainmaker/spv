/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_tools

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo/cm_pbgo"
	"github.com/stretchr/testify/require"
)

func Test_CalcDagHash(t *testing.T) {
	hb, err := CalcDagHash("SHA256", &cmCommonPb.DAG{})
	require.NotNil(t, hb)
	require.Nil(t, err)
}

func Test_CalcRWSetRoot(t *testing.T) {
	ht := "SHA256"
	tx1 := &cmCommonPb.Transaction{
		Result: &cmCommonPb.Result{
			RwSetHash: []byte("dwsdefdwdfdwq3ddedsqw"),
		},
	}
	tx2 := &cmCommonPb.Transaction{
		Result: &cmCommonPb.Result{
			RwSetHash: []byte("dwsdefdwdfdwq3ddedsqw"),
		},
	}
	tx3 := &cmCommonPb.Transaction{
		Result: &cmCommonPb.Result{
			RwSetHash: []byte("dwsdefdwdfdwq3ddedsqw"),
		},
	}

	root, err := CalcRWSetRoot(ht, []*cmCommonPb.Transaction{tx1, tx2, tx3})
	require.NotNil(t, root)
	require.Nil(t, err)
}

func Test_CalcTxHash(t *testing.T) {
	conf.RemoteChainConfigs["chain1"] = &protogo.RemoteConfig{
		HashType: "SHA256",
	}
	th, err := CalcTxHash(&cmCommonPb.Transaction{
		Payload: &cmCommonPb.Payload{
			ChainId: "chain1",
		},
	})
	require.NotNil(t, th)
	require.Nil(t, err)
}

func Test_NewContractInfo(t *testing.T) {
	ci := NewContractInfo("contractname", "method", make([]*cmCommonPb.KeyValuePair, 0))
	require.NotNil(t, ci)
}

func Test_GetContractInfoByTxPayload(t *testing.T) {
	tran := &cmCommonPb.Transaction{
		Payload: &cmCommonPb.Payload{
			ChainId:      "chain1",
			ContractName: "contract1",
			Method:       "unknown",
			Parameters: []*cmCommonPb.KeyValuePair{
				{
					Key:   "key1",
					Value: []byte("value1"),
				},
			},
		},
	}

	ci, err := GetContractInfoByTxPayload(tran)
	require.NotNil(t, ci)
	require.Nil(t, err)
}

func Test_TxRequestConverter(t *testing.T) {
	request := &cm_pbgo.TxRequest{
		Payload: &cm_pbgo.Payload{
			ChainId: "chain1",
		},
	}
	req, err := TxRequestConverter(request)
	require.Nil(t, err)
	require.Equal(t, req.Payload.ChainId, "chain1")
}

func Test_TxResponseConverter(t *testing.T) {
	response := &cmCommonPb.TxResponse{
		Code: cmCommonPb.TxStatusCode_SUCCESS,
	}

	resp, err := TxResponseConverter(response)
	require.Nil(t, err)
	require.Equal(t, resp.Code, cm_pbgo.TxStatusCode_SUCCESS)
}
