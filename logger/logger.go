/*
 Copyright (C) BABEC. All rights reserved.
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package logger contains spv logs for each module
package logger

import (
	"io"
	"log"
	"os"
	"time"

	rotatelogs "chainmaker.org/chainmaker/common/v2/log/file-rotatelogs"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// LogLevel is a type of log level
type LogLevel int

// log level
const (
	LevelDebug LogLevel = iota
	LevelInfo
	LevelWarn
	LevelError
)

// log level
const (
	DEBUG = "DEBUG"
	INFO  = "INFO"
	WARN  = "WARN"
	ERROR = "ERROR"
)

// GetLogLevel gets log level
func GetLogLevel(lvl string) LogLevel {
	switch lvl {
	case ERROR:
		return LevelError
	case WARN:
		return LevelWarn
	case INFO:
		return LevelInfo
	case DEBUG:
		return LevelDebug
	}
	return LevelDebug
}

// default config of log cutting
const (
	DefaultMaxAge       = 365 // maximum log retention time，unit：day
	DefaultRotationTime = 6   // log scrolling interval，unit：hour
)

var hookMap = make(map[string]struct{})

// Config is spv config
type Config struct {
	Module       string   // module: module name
	LogPath      string   // logPath: logger file save path
	LogLevel     LogLevel // logLevel: logger level
	MaxAge       int      // maxAge: the maximum number of days to retain old logger files
	RotationTime int      // RotationTime: rotation time
	JsonFormat   bool     // jsonFormat: logger file use json format
	ShowLine     bool     // showLine: show filename and line number
	LogInConsole bool     // logInConsole: show logs in console at the same time
	ShowColor    bool     // if true, show color logger
}

func initSugarLogger(config *Config) *zap.SugaredLogger {
	var level zapcore.Level
	switch config.LogLevel {
	case LevelDebug:
		level = zap.DebugLevel
	case LevelInfo:
		level = zap.InfoLevel
	case LevelWarn:
		level = zap.WarnLevel
	case LevelError:
		level = zap.ErrorLevel
	default:
		level = zap.InfoLevel
	}
	aLevel := zap.NewAtomicLevel()
	aLevel.SetLevel(level)
	return newLogger(config, aLevel).Sugar()
}

func newLogger(config *Config, level zap.AtomicLevel) *zap.Logger {
	var (
		hook io.Writer
		ok   bool
		err  error
	)
	_, ok = hookMap[config.LogPath]
	if !ok {
		hook, err = getHook(config.LogPath, config.MaxAge, config.RotationTime)
		if err != nil {
			log.Fatalf("new logger get hook failed, %s", err)
		}
		hookMap[config.LogPath] = struct{}{}
	} else {
		hook, err = getHook(config.LogPath, config.MaxAge, config.RotationTime)
		if err != nil {
			log.Fatalf("new logger get hook failed, %s", err)
		}
	}
	var syncer zapcore.WriteSyncer
	if config.LogInConsole {
		syncer = zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(hook))
	} else {
		syncer = zapcore.AddSync(hook)
	}
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:        "time",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "line",
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    customLevelEncoder,
		EncodeTime:     customTimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
		EncodeName:     zapcore.FullNameEncoder,
	}
	var encoder zapcore.Encoder
	if config.JsonFormat {
		encoder = zapcore.NewJSONEncoder(encoderConfig)
	} else {
		encoder = zapcore.NewConsoleEncoder(encoderConfig)
	}
	core := zapcore.NewCore(
		encoder,
		syncer,
		level,
	)
	logger := zap.New(core).Named(config.Module)
	// nolint
	defer logger.Sync()

	if config.ShowLine {
		logger = logger.WithOptions(zap.AddCaller())
	}
	return logger
}

func getHook(filename string, maxAge, rotationTime int) (io.Writer, error) {
	hook, err := rotatelogs.New(
		filename+".%Y%m%d%H",
		rotatelogs.WithLinkName(filename),
		rotatelogs.WithMaxAge(time.Hour*24*time.Duration(maxAge)),
		rotatelogs.WithRotationTime(time.Hour*time.Duration(rotationTime)),
	)
	if err != nil {
		return nil, err
	}
	return hook, nil
}

func customLevelEncoder(level zapcore.Level, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString("[" + level.CapitalString() + "]")
}

func customTimeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.Format("2006-01-02 15:04:05.000"))
}
