#!/usr/bin/env bash

#
# Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

function stop() {
  stop_spv
  if [[ ${ARG1} == "clean" ]] ; then
    clean
  fi
}

function clean() {
  cd ${DUMP_PATH} && rm -rf data && cd - > /dev/null
  cd ${DUMP_PATH} && rm -rf log && cd - > /dev/null
  cd ${DUMP_PATH} && rm -rf bin/system.log && cd - > /dev/null
  echo "Data is Cleaned!"
}

function stop_spv() {
  cd ${DUMP_PATH}/bin && ./stop.sh && cd - > /dev/null
}

# output root dir
DUMP_PATH="../build/release"
ARG1=$1

stop