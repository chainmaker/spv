/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package common

// Transactioner defines the interface that the transaction needs to implement
type Transactioner interface {
	// GetStatusCode returns the transaction status code
	GetStatusCode() int32

	// GetTransactionHash returns transaction hash
	GetTransactionHash() ([]byte, error)

	// GetContractName returns the contract name of transaction
	GetContractName() (string, error)

	// GetMethod returns the method in contract method of transaction
	GetMethod() (string, error)

	// GetParams returns parameters of transaction
	GetParams() ([]interface{}, error)

	// GetExtraData returns extra data of transaction, for example read-write set.
	GetExtraData() (interface{}, error)
}
