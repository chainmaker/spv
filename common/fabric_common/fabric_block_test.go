/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package fabric_common

import (
	"testing"

	"chainmaker.org/chainmaker/spv/v2/utils/fabric_tools"
	fabCommonPb "github.com/hyperledger/fabric-protos-go/common"
	"github.com/stretchr/testify/require"
)

func TestFabBlock_Func(t *testing.T) {
	block := FabBlock{
		ChainId: "mychannel",
		Block:   fabric_tools.NewBlock(),
	}
	header := block.GetBlockHeader()
	require.NotNil(t, header)

	tx := block.GetTransaction()
	require.Equal(t, len(tx), 1)

	extra := block.GetExtraData()
	require.Nil(t, extra)

	chainId := block.GetChainId()
	require.Equal(t, chainId, "mychannel")

	preHash := block.GetPreHash()
	require.Equal(t, preHash, []byte("preHash"))

	txRoot := block.GetTxRoot()
	require.Equal(t, txRoot, fabric_tools.GetBlockDataHash(block.Block.Data))

	height := block.GetHeight()
	require.Equal(t, height, uint64(1))

	blockHash := block.GetBlockHash()
	require.Equal(t, blockHash, fabric_tools.GetBlockHash(block.Block.Header))
}

func TestFabBlockHeader_Func(t *testing.T) {
	header := &FabBlockHeader{
		ChainId: "mychannel",
		BlockHeader: &fabCommonPb.BlockHeader{
			Number:       1,
			PreviousHash: []byte("preHash"),
			DataHash:     []byte("dataHash"),
		},
	}
	chainId := header.GetChainId()
	require.Equal(t, chainId, "mychannel")

	preHash := header.GetPreHash()
	require.Equal(t, string(preHash), "preHash")

	txRoot := header.GetTxRoot()
	require.Equal(t, string(txRoot), "dataHash")

	height := header.GetHeight()
	require.Equal(t, height, uint64(1))

	blockHash := header.GetBlockHash()
	require.NotNil(t, blockHash)
}
