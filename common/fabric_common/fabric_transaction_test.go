/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package fabric_common

import (
	"testing"

	"chainmaker.org/chainmaker/spv/v2/utils/fabric_tools"
	"github.com/stretchr/testify/require"
)

func TestFabTransaction_Func(t *testing.T) {
	fabTx := &FabTransaction{
		Transaction:    fabric_tools.NewEnvelope(),
		ValidationCode: int32(0),
	}

	sc := fabTx.GetStatusCode()
	require.Equal(t, sc, int32(0))

	hash, err := fabTx.GetTransactionHash()
	require.NotNil(t, hash)
	require.Nil(t, err)

	name, err := fabTx.GetContractName()
	require.Equal(t, name, "myncc")
	require.Nil(t, err)

	method, err := fabTx.GetMethod()
	require.Equal(t, method, "invoke")
	require.Nil(t, err)

	args, err := fabTx.GetParams()
	require.Equal(t, len(args), 3)
	require.Nil(t, err)

	extra, err := fabTx.GetExtraData()
	require.Nil(t, extra)
	require.NotNil(t, err)
}
