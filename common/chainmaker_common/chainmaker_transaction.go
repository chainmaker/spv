/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_common

import (
	"errors"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/utils/chainmaker_tools"
)

// CMTransaction packages ChainMaker's Transaction
type CMTransaction struct {
	BlockVersion uint32
	Transaction  *cmCommonPb.Transaction
	RWSet        *cmCommonPb.TxRWSet // extra data in the transaction for ChainMaker light
}

// GetStatusCode returns the transaction status code
func (tx *CMTransaction) GetStatusCode() int32 {
	return int32(tx.Transaction.GetResult().GetCode())
}

// GetTransactionHash returns transaction hash
func (tx *CMTransaction) GetTransactionHash() ([]byte, error) {
	return chainmaker_tools.CalcTxHashWithVersion(tx.Transaction, int(tx.BlockVersion))
}

// GetContractName returns the contract name of transaction
func (tx *CMTransaction) GetContractName() (string, error) {
	if tx.Transaction == nil {
		return "", errors.New("transaction should not be nil")
	}
	if tx.Transaction.Payload == nil {
		return "", errors.New("the payload in transaction should not be nil")
	}
	return tx.Transaction.Payload.ContractName, nil
}

// GetMethod returns the method in contract method of transaction
func (tx *CMTransaction) GetMethod() (string, error) {
	if tx.Transaction == nil {
		return "", errors.New("transaction should not be nil")
	}
	if tx.Transaction.Payload == nil {
		return "", errors.New("the payload in transaction should not be nil")
	}
	return tx.Transaction.Payload.Method, nil
}

// GetParams returns parameters of transaction
func (tx *CMTransaction) GetParams() ([]interface{}, error) {
	if tx.Transaction == nil {
		return nil, errors.New("transaction should not be nil")
	}
	if tx.Transaction.Payload == nil {
		return nil, errors.New("the payload in transaction should not be nil")
	}
	params := make([]interface{}, len(tx.Transaction.Payload.Parameters))
	for i := 0; i < len(tx.Transaction.Payload.Parameters); i++ {
		params[i] = tx.Transaction.Payload.Parameters[i]
	}
	return params, nil
}

// GetExtraData returns extra data(rw_set) of transaction
func (tx *CMTransaction) GetExtraData() (interface{}, error) {
	return tx.RWSet, nil
}
