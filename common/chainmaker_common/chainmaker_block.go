/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_common

import (
	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common"
)

// CMBlock packages ChainMaker's block
type CMBlock struct {
	Block     *cmCommonPb.Block
	RWSetList []*cmCommonPb.TxRWSet // extra data in the block
}

// GetBlockHeader returns block header
func (cmb *CMBlock) GetBlockHeader() common.Header {
	return &CMBlockHeader{
		BlockHeader: cmb.Block.GetHeader(),
	}
}

// GetTransaction returns transactions in the block
func (cmb *CMBlock) GetTransaction() []common.Transactioner {
	if cmb.Block == nil {
		return nil
	}
	if cmb.Block.Txs == nil {
		return nil
	}
	var txer = make([]common.Transactioner, len(cmb.Block.Txs))
	for i := 0; i < len(cmb.Block.Txs); i++ {
		tx := &CMTransaction{
			Transaction: cmb.Block.Txs[i],
		}
		if cmb.RWSetList != nil {
			tx.RWSet = cmb.RWSetList[i]
		}
		txer[i] = tx
	}
	return txer
}

// GetExtraData returns RWSetList in the block
func (cmb *CMBlock) GetExtraData() interface{} {
	return cmb.RWSetList
}

// GetChainId returns chainId
func (cmb *CMBlock) GetChainId() string {
	return cmb.Block.GetHeader().GetChainId()
}

// GetPreHash returns the hash value of previous block
func (cmb *CMBlock) GetPreHash() []byte {
	return cmb.Block.GetHeader().GetPreBlockHash()
}

// GetTxRoot returns the root hash of transaction tree
func (cmb *CMBlock) GetTxRoot() []byte {
	return cmb.Block.GetHeader().GetTxRoot()
}

// GetHeight returns block height
func (cmb *CMBlock) GetHeight() uint64 {
	return cmb.Block.GetHeader().GetBlockHeight()
}

// GetBlockHash returns block hash
func (cmb *CMBlock) GetBlockHash() []byte {
	return cmb.Block.GetHeader().GetBlockHash()
}

// CMBlockHeader packages ChainMaker's block header
type CMBlockHeader struct {
	BlockHeader *cmCommonPb.BlockHeader
}

// GetChainId returns chainId
func (cmh *CMBlockHeader) GetChainId() string {
	return cmh.BlockHeader.GetChainId()
}

// GetPreHash returns the hash value of previous block
func (cmh *CMBlockHeader) GetPreHash() []byte {
	return cmh.BlockHeader.GetPreBlockHash()
}

// GetTxRoot returns the root hash of transaction tree
func (cmh *CMBlockHeader) GetTxRoot() []byte {
	return cmh.BlockHeader.GetTxRoot()
}

// GetHeight returns block height
func (cmh *CMBlockHeader) GetHeight() uint64 {
	return cmh.BlockHeader.GetBlockHeight()
}

// GetBlockHash returns block hash
func (cmh *CMBlockHeader) GetBlockHash() []byte {
	return cmh.BlockHeader.GetBlockHash()
}
