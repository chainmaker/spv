/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package leveldb

import (
	"testing"

	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/storage"
	"github.com/stretchr/testify/require"
)

func TestStateDB_Func(t *testing.T) {
	stateDB := NewKvStateDB(&conf.StoreConfig{
		Provider: "leveldb",
		LevelDB: &conf.LevelDBConfig{
			StorePath:       "../data/spv_db",
			WriteBufferSize: 4,
			BloomFilterBits: 10,
		},
	}, nil)
	chainID := "chain1"
	height := uint64(1)
	txHashs := map[string][]byte{
		"tx1": []byte("tx1's hash"),
		"tx2": []byte("tx2's hash"),
		"tx3": []byte("tx3's hash"),
	}
	txs := map[string][]byte{
		"tx1": []byte("tx1"),
		"tx2": []byte("tx2"),
		"tx3": []byte("tx3"),
	}
	extra := map[string][]byte{
		"tx1": []byte("tx1's extra"),
		"tx2": []byte("tx2's extra"),
		"tx3": []byte("tx3's extra"),
	}

	bd := storage.NewBlockData([]byte("block Hash"), []byte("chain1 Header"))
	td := storage.NewTransactionData()
	td.TxHashMap = txHashs
	td.TxBytesMap = txs
	td.ExtraDataMap = extra

	err := stateDB.CommitBlockDataAndTxData(chainID, uint64(1), bd, td)
	require.Nil(t, err)

	bh, ok := stateDB.GetBlockHeaderByHeight(chainID, height)
	require.NotNil(t, bh)
	require.Nil(t, err)

	bh, h, ok := stateDB.GetBlockHeaderAndHeightByHash(chainID, []byte("block Hash"))
	require.NotNil(t, bh)
	require.Equal(t, h, uint64(1))
	require.Nil(t, err)

	bh, h, ok = stateDB.GetLastCommittedBlockHeaderAndHeight(chainID)
	require.NotNil(t, bh)
	require.Equal(t, h, uint64(1))
	require.Equal(t, ok, true)

	tx, h, ok := stateDB.GetTransactionHashAndHeightByTxKey(chainID, "tx1")
	require.NotNil(t, tx)
	require.Equal(t, h, uint64(1))
	require.Equal(t, ok, true)

	tx, h, ok = stateDB.GetTransactionHashAndHeightByTxKey(chainID, "tx2")
	require.NotNil(t, tx)
	require.Equal(t, h, uint64(1))
	require.Equal(t, ok, true)

	tx, h, ok = stateDB.GetTransactionHashAndHeightByTxKey(chainID, "tx3")
	require.NotNil(t, tx)
	require.Equal(t, h, uint64(1))
	require.Equal(t, ok, true)

	tran, ok := stateDB.GetTransactionByTxKey(chainID, "tx3")
	require.NotNil(t, tran)
	require.Equal(t, ok, true)

	rw, ok := stateDB.GetTxExtraDataByTxKey(chainID, "tx3")
	require.NotNil(t, rw)
	require.Equal(t, ok, true)

	tt, ok := stateDB.GetTransactionTotalNum(chainID)
	require.Greater(t, tt, uint64(2))
	require.Equal(t, ok, true)

	err = stateDB.WriteChainConfig(chainID, []byte("chain1_config"))
	require.Nil(t, err)

	cc, ok := stateDB.GetChainConfig(chainID)
	require.NotNil(t, cc)
	require.Nil(t, err)

	stateDB.Close()
}
