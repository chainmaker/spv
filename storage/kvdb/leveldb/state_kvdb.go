/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package leveldb is leveldb module
package leveldb

import (
	"fmt"
	"strconv"

	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/storage"
	"chainmaker.org/chainmaker/spv/v2/storage/kvdb"
	"go.uber.org/zap"
)

const (
	BlockHeaderPrefix              = "BH/%s"  // BH/chainId+height
	BlockHeightPrefix              = "BH/%s"  // BH/chainId+blockHash
	LastCommittedBlockHeaderPrefix = "LB/%s"  // LB/chainId
	LastCommittedBlockHeightPrefix = "LH/%s"  // LH/chainId
	TxHashPrefix                   = "THA/%s" // THA/chainId+txKey
	TxHeightPrefix                 = "THE/%s" // THE/chainId+txKey
	TxPrefix                       = "TX/%s"  // TX/chainId+txKey
	TxExtraDataPrefix              = "TE/%s"  // TE/chainId+txKey
	TxTotalNumPrefix               = "TT/%s"  // TE/chainId
	ChainConfigPrefix              = "CC/%s"  // CC/chainId
)

// KvStateDB is the implementation of StateDB interface
type KvStateDB struct {
	provider kvdb.Provider
	log      *zap.SugaredLogger
}

// NewKvStateDB creates KvStateDB
func NewKvStateDB(storeConfig *conf.StoreConfig, log *zap.SugaredLogger) *KvStateDB {
	if log == nil {
		log = logger.GetLogger(logger.ModuleStateDB)
	}
	kvStateDB := &KvStateDB{
		provider: NewLevelDBProvider(storeConfig.LevelDB),
		log:      log,
	}
	return kvStateDB
}

// CommitBlockDataAndTxData commits block data and transaction data in an atomic operation
func (db *KvStateDB) CommitBlockDataAndTxData(chainId string, height uint64,
	blockData *storage.BlockData, txData *storage.TransactionData) error {
	batch := kvdb.NewKvDBBatcher()
	// add serializedHeader
	blockHeaderKey := blockHeaderKey(chainId, strconv.FormatUint(height, 10))
	batch.Add(blockHeaderKey, blockData.SerializedHeader)
	// add block height
	blockHeightKey := blockHeightKey(chainId, string(blockData.BlockHash))
	batch.Add(blockHeightKey, []byte(strconv.FormatUint(height, 10)))
	// add txHash and txHeight
	for txKey, txHash := range txData.TxHashMap {
		txHashKey := txHashKey(chainId, txKey)
		txHeightKey := txHeightKey(chainId, txKey)
		batch.Add(txHashKey, txHash)
		batch.Add(txHeightKey, []byte(strconv.FormatUint(height, 10)))
	}
	// add txBytes and calc the total num of transaction
	var (
		num = int64(0)
		err error
	)
	txNum, ok := db.provider.Get(txTotalNumKey(chainId))
	if ok {
		num, err = strconv.ParseInt(string(txNum), 10, 64)
		if err != nil {
			db.log.Errorf("[chainId:%s] parse int failed, err:%v", chainId, err)
			num = int64(0)
		}
	}
	for txKey, txBytes := range txData.TxBytesMap {
		tranKey := transactionKey(chainId, txKey)
		batch.Add(tranKey, txBytes)
		num++
	}
	// add extra data bytes
	for txKey, data := range txData.ExtraDataMap {
		txExtraKey := txExtraDataKey(chainId, txKey)
		batch.Add(txExtraKey, data)
	}
	// add last committed block header and height, and the total number of tx
	batch.Add(lastCommittedBlockHeaderKey(chainId), blockData.SerializedHeader)
	batch.Add(lastCommittedBlockHeightKey(chainId), []byte(strconv.FormatUint(height, 10)))
	batch.Add(txTotalNumKey(chainId), []byte(strconv.FormatInt(num, 10)))
	err = db.provider.WriteBatch(batch)
	if err != nil {
		db.log.Errorf("[chainId:%s] commit block header and transaction data failed, block height:%d", chainId, height)
		return err
	}
	return nil
}

// GetBlockHeaderByHeight returns the serialized block header given it's height, or returns nil if none exists
func (db *KvStateDB) GetBlockHeaderByHeight(chainId string, height uint64) ([]byte, bool) {
	blockHeaderKey := blockHeaderKey(chainId, strconv.FormatUint(height, 10))
	header, ok := db.provider.Get(blockHeaderKey)
	if !ok {
		db.log.Debugf("[chainId:%s] get block header by height failed, block height:%d", chainId, height)
		return nil, false
	}
	return header, true
}

// GetBlockHeaderAndHeightByHash returns the serialized block header and height given it's block hash,
// or returns nil if none exists
func (db *KvStateDB) GetBlockHeaderAndHeightByHash(chainId string, blockHash []byte) ([]byte, uint64, bool) {
	blockHeightKey := blockHeightKey(chainId, string(blockHash))
	// get height by hash
	blockHeight, ok := db.provider.Get(blockHeightKey)
	if !ok {
		db.log.Warnf("[chainId:%s] get block height by hash failed, block hash:%x", chainId, blockHash)
		return nil, 0, false
	}
	height, err := strconv.ParseUint(string(blockHeight), 10, 64)
	if err != nil {
		db.log.Errorf("[chainId:%s] parse uint failed, err:%v", chainId, err)
		return nil, 0, false
	}
	// get serialized header by height
	blockHeaderKey := blockHeaderKey(chainId, strconv.FormatUint(height, 10))
	header, ok := db.provider.Get(blockHeaderKey)
	if !ok {
		db.log.Warnf("[chainId:%s] get block header by hash and height failed, block hash:%v, block height:%d",
			chainId, blockHash, height)
		return nil, 0, false
	}
	return header, height, true
}

// GetLastCommittedBlockHeaderAndHeight returns the last committed block header,or returns nil if none exists
func (db *KvStateDB) GetLastCommittedBlockHeaderAndHeight(chainId string) ([]byte, uint64, bool) {
	lastCommittedBlockHeader, ok := db.provider.Get(lastCommittedBlockHeaderKey(chainId))
	if !ok {
		db.log.Debugf("[chainId:%s] get last committed block header failed", chainId)
		return nil, 0, false
	}
	lastCommittedBlockHeight, ok := db.provider.Get(lastCommittedBlockHeightKey(chainId))
	if !ok {
		db.log.Debugf("[chainId:%s] get last committed block height failed", chainId)
		return nil, 0, false
	}
	height, err := strconv.ParseUint(string(lastCommittedBlockHeight), 10, 64)
	if err != nil {
		db.log.Errorf("[chainId:%s] parse uint failed, err:%v", chainId, err)
		return nil, 0, false
	}
	return lastCommittedBlockHeader, height, true
}

// GetTransactionHashAndHeightByTxKey returns transaction hash and block height by txId or txHash,
// or returns nil if none exists
func (db *KvStateDB) GetTransactionHashAndHeightByTxKey(chainId string, txKey string) ([]byte, uint64, bool) {
	txHashKey := txHashKey(chainId, txKey)
	txHeightKey := txHeightKey(chainId, txKey)
	txHash, ok := db.provider.Get(txHashKey)
	if !ok {
		db.log.Warnf("[chainId:%s] get transaction hash by txKey failed, transaction key:%s", chainId, txKey)
		return nil, 0, false
	}
	txHeight, ok := db.provider.Get(txHeightKey)
	if !ok {
		db.log.Warnf("[chainId:%s] get transaction height by txKey failed, transaction key:%s", chainId, txKey)
		return nil, 0, false
	}
	height, err := strconv.ParseUint(string(txHeight), 10, 64)
	if err != nil {
		db.log.Errorf("[chainId:%s] parse uint failed, err:%v", chainId, err)
		return nil, 0, false
	}
	return txHash, height, true
}

// GetTransactionByTxKey returns transaction bytes by txId, or returns nil if none exists
func (db *KvStateDB) GetTransactionByTxKey(chainId string, txKey string) ([]byte, bool) {
	tx, ok := db.provider.Get(transactionKey(chainId, txKey))
	if !ok {
		db.log.Warnf("[chainId:%s] get transaction by txKey failed", chainId)
		return nil, false
	}
	return tx, true
}

// GetTxExtraDataByTxKey returns transaction extra data (rw_set) by txId, or returns nil if none exists
func (db *KvStateDB) GetTxExtraDataByTxKey(chainId string, txKey string) ([]byte, bool) {
	extra, ok := db.provider.Get(txExtraDataKey(chainId, txKey))
	if !ok {
		db.log.Warnf("[chainId:%s] get transaction extra data(rwset) by txKey failed", chainId)
		return nil, false
	}
	return extra, true
}

// GetTransactionTotalNum returns the total num of transactions, or returns 0 if none exists
func (db *KvStateDB) GetTransactionTotalNum(chainId string) (uint64, bool) {
	totalNumKey := txTotalNumKey(chainId)
	total, ok := db.provider.Get(totalNumKey)
	if !ok {
		db.log.Warnf("[chainId:%s] get the total num of transactions failed", chainId)
		return 0, false
	}
	num, err := strconv.ParseUint(string(total), 10, 64)
	if err != nil {
		db.log.Errorf("[chainId:%s] parse uint failed, err:%v", chainId, err)
		return 0, false
	}
	return num, true
}

// WriteChainConfig put the chain config to db
func (db *KvStateDB) WriteChainConfig(chainId string, chainConfig []byte) error {
	err := db.provider.Put(chainConfigKey(chainId), chainConfig)
	if err != nil {
		db.log.Warnf("[chainId:%s] write chain conf failed", chainId)
		return err
	}
	return nil
}

// GetChainConfig returns chain config, or returns nil if none exists
func (db *KvStateDB) GetChainConfig(chainId string) ([]byte, bool) {
	cc, ok := db.provider.Get(chainConfigKey(chainId))
	if !ok {
		db.log.Warnf("[chainId:%s] get chain conf failed", chainId)
		return nil, false
	}
	return cc, true
}

// Close is used to close database
func (db *KvStateDB) Close() {
	db.provider.Close()
}

func blockHeaderKey(chainId string, height string) string {
	return fmt.Sprintf(BlockHeaderPrefix, chainId+height)
}

func blockHeightKey(chainId string, blockHash string) string {
	return fmt.Sprintf(BlockHeightPrefix, chainId+blockHash)
}

func txHashKey(chainId string, txKey string) string {
	return fmt.Sprintf(TxHashPrefix, chainId+txKey)
}

func txHeightKey(chainId string, txKey string) string {
	return fmt.Sprintf(TxHeightPrefix, chainId+txKey)
}

func transactionKey(chainId string, txKey string) string {
	return fmt.Sprintf(TxPrefix, chainId+txKey)
}

func txExtraDataKey(chainId string, txKey string) string {
	return fmt.Sprintf(TxExtraDataPrefix, chainId+txKey)
}

func txTotalNumKey(chainId string) string {
	return fmt.Sprintf(TxTotalNumPrefix, chainId)
}

func lastCommittedBlockHeaderKey(chainId string) string {
	return fmt.Sprintf(LastCommittedBlockHeaderPrefix, chainId)
}

func lastCommittedBlockHeightKey(chainId string) string {
	return fmt.Sprintf(LastCommittedBlockHeightPrefix, chainId)
}

func chainConfigKey(chainId string) string {
	return fmt.Sprintf(ChainConfigPrefix, chainId)
}
