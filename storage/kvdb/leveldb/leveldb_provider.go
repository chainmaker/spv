/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package leveldb

import (
	"errors"
	"fmt"
	"sync"

	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/storage/kvdb"
	"github.com/btcsuite/goleveldb/leveldb"
	"github.com/btcsuite/goleveldb/leveldb/filter"
	"github.com/btcsuite/goleveldb/leveldb/opt"
)

const (
	defaultWriteBufferSize = 4
	defaultBloomFilterBits = 10
)

// LevelDBProvider provides handle to db instances
type LevelDBProvider struct { // nolint
	sync.Mutex
	db *leveldb.DB
	wo *opt.WriteOptions
}

// NewLevelDBProvider construct a new Provider for state operation with given config
func NewLevelDBProvider(ldbConfig *conf.LevelDBConfig) *LevelDBProvider {
	var (
		dbOpts          = &opt.Options{}
		dbPath          string
		writeBufferSize int
		bloomFilterBits int
	)

	if ldbConfig != nil {
		dbPath = ldbConfig.StorePath
		writeBufferSize = ldbConfig.WriteBufferSize
		bloomFilterBits = ldbConfig.BloomFilterBits
	} else {
		writeBufferSize = conf.SPVConfig.StorageConfig.LevelDB.WriteBufferSize
		bloomFilterBits = conf.SPVConfig.StorageConfig.LevelDB.BloomFilterBits
		dbPath = conf.SPVConfig.StorageConfig.LevelDB.StorePath
	}
	if writeBufferSize <= 0 {
		writeBufferSize = defaultWriteBufferSize
	}
	dbOpts.WriteBuffer = writeBufferSize * opt.MiB
	if bloomFilterBits <= 0 {
		bloomFilterBits = defaultBloomFilterBits
	}
	dbOpts.Filter = filter.NewBloomFilter(bloomFilterBits)
	db, err := leveldb.OpenFile(dbPath, dbOpts)
	if err != nil {
		panic(fmt.Sprintf("Error opening leveldbprovider: %s", err))
	}
	return &LevelDBProvider{
		db: db,
		wo: &opt.WriteOptions{Sync: true},
	}
}

func (l *LevelDBProvider) Get(key string) ([]byte, bool) {
	value, err := l.db.Get([]byte(key), nil)
	if err != nil {
		return nil, false
	}
	return value, true
}

func (l *LevelDBProvider) Put(key string, value []byte) error {
	if key == "" {
		return errors.New("error writing leveldb with nil key")
	}
	if value == nil {
		return errors.New("error writing leveldb with nil value")
	}
	err := l.db.Put([]byte(key), value, l.wo)
	if err != nil {
		return err
	}
	return nil
}

func (l *LevelDBProvider) Has(key string) (bool, error) {
	exist, err := l.db.Has([]byte(key), nil)
	if err != nil {
		return false, err
	}
	return exist, nil
}

func (l *LevelDBProvider) Delete(key string) error {
	err := l.db.Delete([]byte(key), l.wo)
	if err != nil {
		return err
	}
	return nil
}

func (l *LevelDBProvider) WriteBatch(batch *kvdb.Batcher) error {
	if batch.Len() == 0 {
		return errors.New("error writing with nil batch")
	}
	levelBatch := &leveldb.Batch{}
	for _, v := range batch.GetKvs() {
		dbKey, dbValue := v.GetKey(), v.GetValue()
		if dbValue == nil {
			// 表示删除
			levelBatch.Delete([]byte(dbKey))
		} else {
			levelBatch.Put([]byte(dbKey), dbValue)
		}
	}
	err := l.db.Write(levelBatch, l.wo)
	if err != nil {
		return err
	}
	return nil
}

func (l *LevelDBProvider) Close() {
	l.db.Close()
}
