/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_prover

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/coder/chainmaker_coder"
	"chainmaker.org/chainmaker/spv/v2/common/chainmaker_common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/manager"
	"chainmaker.org/chainmaker/spv/v2/mock"
	"chainmaker.org/chainmaker/spv/v2/pb/api"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"chainmaker.org/chainmaker/spv/v2/storage"
	"chainmaker.org/chainmaker/spv/v2/storage/kvdb/leveldb"
	"chainmaker.org/chainmaker/spv/v2/utils/chainmaker_tools"
	"chainmaker.org/chainmaker/spv/v2/verifier/chainmaker_verifier"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestChainMakerProver_Func(t *testing.T) {
	chainID := "chain1"
	blockVersion := 2300
	cc := &conf.ChainConfig{
		ChainId:       chainID,
		SyncInterval:  1000,
		BlockInterval: 10,
		SDKConfigPath: "../config/chainmaker_sdk_config_chain1.yml",
	}
	conf.SPVConfig = &conf.LocalConfig{
		Chains: []*conf.ChainConfig{
			cc,
		},
		StorageConfig: &conf.StoreConfig{
			Provider: "leveldb",
			LevelDB: &conf.LevelDBConfig{
				StorePath:       "../data/spv_db",
				WriteBufferSize: 4,
				BloomFilterBits: 10,
			},
		},
	}
	conf.RemoteChainConfigs[chainID] = &protogo.RemoteConfig{
		HashType: "SHA256",
	}
	tx := &cmCommonPb.Transaction{
		Payload: &cmCommonPb.Payload{
			ChainId:      chainID,
			ContractName: "name",
			Method:       "method",
			Parameters: []*cmCommonPb.KeyValuePair{
				{
					Key:   "key1",
					Value: []byte("value1"),
				},
			},
		},
	}
	txHash, _ := chainmaker_tools.CalcTxHashWithVersion(tx, blockVersion)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	sdkAdapter := mock.NewMockSDKAdapter(ctrl)
	sdkAdapter.EXPECT().GetTransactionByTxKey(gomock.Eq("tx1")).Return(&chainmaker_common.CMTransaction{Transaction: tx, BlockVersion: uint32(blockVersion)}, nil).AnyTimes()
	spvCoder := chainmaker_coder.NewCMSPVCoder()
	store := leveldb.NewKvStateDB(conf.SPVConfig.StorageConfig, nil)
	cmStateManager := manager.NewStateManager(conf.SPVConfig.Chains[0].ChainId, conf.SPVConfig.Chains[0].BlockInterval, sdkAdapter, chainmaker_verifier.NewCMSPVVerifier(), spvCoder, store, nil)
	chainProver := NewChainMakerProver(chainID, sdkAdapter, spvCoder, store, cmStateManager, nil)

	bd := storage.NewBlockData([]byte("block Hash"), []byte("chain1 Header"))
	td := &storage.TransactionData{
		TxHashMap: map[string][]byte{
			"tx1": txHash,
			"tx2": []byte("tx2's hash"),
			"tx3": []byte("tx3's hash"),
		},
		TxBytesMap:   nil,
		ExtraDataMap: nil,
	}
	store.CommitBlockDataAndTxData(chainID, 2, bd, td)
	err := chainProver.VerifyTransaction(&api.TxValidationRequest{
		ChainId:     chainID,
		TxKey:       "tx1",
		BlockHeight: 2,
		ContractData: &api.ContractData{
			Name:   "name",
			Method: "method",
			Params: []*api.KVPair{
				{
					Key:   "key1",
					Value: []byte("value1"),
				},
			},
		},
		Timeout: 5000,
	})
	require.Nil(t, err)
}
