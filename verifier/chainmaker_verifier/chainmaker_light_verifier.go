/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_verifier

import (
	"errors"
	"fmt"
	"reflect"

	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common/chainmaker_common"
)

// CMLightVerifier implements block verifier interface for ChainMaker Light
type CMLightVerifier struct {
	cmVerifier *cmVerifier
}

// NewCMLightVerifier creates CMLightVerifier
func NewCMLightVerifier() *CMLightVerifier {
	return &CMLightVerifier{
		cmVerifier: newCMVerifier(),
	}
}

// ValidBlock verifies the validity of the block for ChainMaker Light
func (cms *CMLightVerifier) ValidBlock(blocker common.Blocker, preBlockHash []byte) error {
	if blocker == nil || preBlockHash == nil {
		return errors.New("blocker or pre block hash is nil")
	}
	cmBlock, ok := blocker.(*chainmaker_common.CMBlock)
	if !ok {
		return fmt.Errorf("assert CMBlock failed, get type:{%s}, want type:{%s}",
			reflect.TypeOf(blocker), reflect.TypeOf(chainmaker_common.CMBlock{}))
	}
	return cms.cmVerifier.validBlock(cmBlock.Block, preBlockHash, true)
}
