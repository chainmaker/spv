/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_verifier

import (
	"bytes"
	"fmt"

	"chainmaker.org/chainmaker/common/v2/crypto/hash"
	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/utils/chainmaker_tools"
)

// cmVerifier implements block verifier interface for ChainMaker
type cmVerifier struct{}

// newCMVerifier creates cmVerifier
func newCMVerifier() *cmVerifier {
	return &cmVerifier{}
}

// ValidBlock verifies the validity of the block for ChainMaker
func (cmv *cmVerifier) validBlock(block *cmCommonPb.Block, preBlockHash []byte, onlyHeader bool) error {
	// 1.verify header
	if !bytes.Equal(preBlockHash, block.GetHeader().GetPreBlockHash()) {
		return fmt.Errorf("block pre hash is unequal, actual pre hash:%x, pre hash in block:%x",
			preBlockHash, block.GetHeader().GetPreBlockHash())
	}
	// if only verify header, return nil.
	if onlyHeader {
		return nil
	}
	// 2.verify body
	// If the block is empty, there is no need to validate other fields in the block
	if block.Header.TxCount == 0 {
		return nil
	}
	// If the block is not empty, three trees will be validated
	if block.Header.TxCount != uint32(len(block.Txs)) {
		return fmt.Errorf("the num of tx is unequal, block height:%d, tx num in header:%d, tx num in body:%d",
			block.Header.BlockHeight, block.Header.TxCount, len(block.Txs))
	}
	hashType := conf.RemoteChainConfigs[block.Header.ChainId].HashType
	txHashes := make([][]byte, 0)
	// verify TxRoot
	for _, tx := range block.Txs {
		txHash, err := chainmaker_tools.CalcTxHashWithVersion(tx, int(block.Header.BlockVersion))
		if err != nil {
			return fmt.Errorf("calc tx hash failed, block height:%d, txId:%x, err:%v",
				block.Header.BlockHeight, tx.Payload.TxId, err)
		}
		txHashes = append(txHashes, txHash)
	}
	txRoot, err := hash.GetMerkleRoot(hashType, txHashes)
	if err != nil {
		return fmt.Errorf("calc merkleroot failed, block height:%d, block hash:%x, err: %s",
			block.Header.BlockHeight, block.Header.BlockHash, err)
	}
	if !bytes.Equal(txRoot, block.GetHeader().GetTxRoot()) {
		return fmt.Errorf("tx merkleroot is unequal, block height:%d, txroot in header:%x, calced txroot:%x",
			block.Header.BlockHeight, block.Header.TxRoot, txRoot)
	}
	// verify DAG hash root
	dagHash, err := chainmaker_tools.CalcDagHash(hashType, block.GetDag())
	if err != nil {
		return fmt.Errorf("calc daghash failed, block height:%d, block hash:%x, err: %s",
			block.Header.BlockHeight, block.Header.BlockHash, err)
	}
	if !bytes.Equal(dagHash, block.Header.DagHash) {
		return fmt.Errorf("daghash is unequal, block height:%d, daghash in header:%x, calced dag hash:%x",
			block.Header.BlockHeight, block.Header.DagHash, dagHash)
	}
	// verify rw_set root
	rwSetRoot, err := chainmaker_tools.CalcRWSetRoot(hashType, block.GetTxs())
	if err != nil {
		return fmt.Errorf("calc rwsetroot failed, block height:%d, block hash:%x, err: %s",
			block.Header.BlockHeight, block.Header.BlockHash, err)
	}
	if !bytes.Equal(rwSetRoot, block.Header.RwSetRoot) {
		return fmt.Errorf("rwsetroot unequal, block height:%d, block hash:%x, rwsetroot in header:%x, rwsetroot:%x",
			block.Header.BlockHeight, block.Header.BlockHash, block.Header.RwSetRoot, rwSetRoot)
	}
	return nil
}
