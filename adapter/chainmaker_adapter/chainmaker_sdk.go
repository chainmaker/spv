/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_adapter

import (
	"context"
	"fmt"

	"chainmaker.org/chainmaker/sdk-go/v2/utils"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common/chainmaker_common"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"go.uber.org/zap"
)

const (
	subscribeBlockChanSize = 2048 // default size of block chan subscribed by spv
)

// cmSDK contains ChainMaker's sdk func
type cmSDK struct {
	chainClient sdk.SDKInterface
}

// newCMSDK creates cmSDK with ymlFile path and log
func newCMSDK(chainId string, ymlFile string, log *zap.SugaredLogger) (*cmSDK, error) {
	// verify chainId
	configModel, err := utils.InitConfig(ymlFile)
	if err != nil {
		return nil, err
	}
	if chainId != configModel.ChainClientConfig.ChainId {
		return nil, fmt.Errorf("chainId is not equal with sdk config")
	}
	// create sdk
	cc, err := createChainClientWithConfig(ymlFile, log)
	if err != nil {
		return nil, err
	}
	return &cmSDK{
		chainClient: cc,
	}, nil
}

// subscribeBlock subscribes to the latest block generated by the remote ChainMaker chain
func (cs *cmSDK) subscribeBlock(withRwSet, onlyHeader bool) (chan common.Blocker, error) {
	ch := make(chan common.Blocker, subscribeBlockChanSize)
	// subscribe to the latest blocks in real time without read_write_set
	chCM, err := cs.chainClient.SubscribeBlock(context.Background(), -1, -1, withRwSet, onlyHeader)
	if err != nil {
		return nil, err
	}
	go func() {
		for {
			blockInfo, ok := <-chCM
			// SubscribeBlock channel is closed
			if !ok {
				close(ch)
				return
			}
			bf, ok := blockInfo.(*cmCommonPb.BlockInfo)
			if !ok {
				continue
			}
			cmBlock := &chainmaker_common.CMBlock{Block: bf.Block}
			if withRwSet {
				cmBlock.RWSetList = bf.RwsetList
			}
			ch <- cmBlock
		}
	}()
	return ch, nil
}

// getChainHeight gets the height of the remote ChainMaker chain
func (cs *cmSDK) getChainHeight() (uint64, error) {
	h, err := cs.chainClient.GetCurrentBlockHeight()
	if err != nil {
		return 0, err
	}
	return h, nil
}

// getBlockByHeight gets the block of a specific height from remote ChainMaker chain
func (cs *cmSDK) getBlockByHeight(blockHeight uint64, withRwSet bool) (common.Blocker, error) {
	blockInfo, err := cs.chainClient.GetBlockByHeight(blockHeight, withRwSet)
	if err != nil {
		return nil, err
	}
	cmBlock := &chainmaker_common.CMBlock{Block: blockInfo.GetBlock()}
	if withRwSet {
		cmBlock.RWSetList = blockInfo.RwsetList
	}
	return cmBlock, nil
}

// getTransactionByTxKey gets the transaction by txId from remote ChainMaker chain
func (cs *cmSDK) getTransactionByTxKey(txKey string) (common.Transactioner, error) {
	// get tx
	transactionInfo, err := cs.chainClient.GetTxByTxId(txKey)
	if err != nil {
		return nil, err
	}
	// get block version from ChainMaker v2.3.0
	header, err := cs.chainClient.GetBlockHeaderByHeight(transactionInfo.BlockHeight)
	if err != nil {
		return nil, err
	}
	return &chainmaker_common.CMTransaction{
		Transaction:  transactionInfo.GetTransaction(),
		BlockVersion: header.BlockVersion,
	}, nil
}

// getChainConfig gets the config of remote ChainMaker chain
func (cs *cmSDK) getChainConfig() (*protogo.RemoteConfig, error) {
	cmChainConfig, err := cs.chainClient.GetChainConfig()
	if err != nil {
		return nil, err
	}
	return &protogo.RemoteConfig{
		ChainId:  cmChainConfig.ChainId,
		HashType: cmChainConfig.Crypto.Hash,
	}, nil
}

// stop closes connection pool and release resource
func (cs *cmSDK) stop() error {
	return cs.chainClient.Stop()
}

func createChainClientWithConfig(ymlFile string, log *zap.SugaredLogger) (*sdk.ChainClient, error) {
	if log == nil {
		log = logger.GetCMSDKLogger()
	}
	cc, err := sdk.NewChainClient(
		sdk.WithConfPath(ymlFile),
		sdk.WithChainClientLogger(log),
	)
	if err != nil {
		return nil, err
	}
	return cc, nil
}

// forwardTransaction forwards transaction to remote chain
func (cs *cmSDK) forwardTransaction(txRequest *cmCommonPb.TxRequest) (*cmCommonPb.TxResponse, error) {
	switch txRequest.Payload.TxType {
	case cmCommonPb.TxType_QUERY_CONTRACT:
		resp, err := cs.chainClient.SendTxRequest(txRequest, -1, true)
		if err != nil {
			return nil, err
		}
		return resp, nil
	case cmCommonPb.TxType_INVOKE_CONTRACT, cmCommonPb.TxType_ARCHIVE:
		resp, err := cs.chainClient.SendTxRequest(txRequest, -1, false)
		if err != nil {
			return nil, err
		}
		return resp, nil
	default:
		return nil, fmt.Errorf("unsupport TxType, type:%d", int(txRequest.Payload.TxType))
	}
}
