/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_adapter

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/config"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/mock"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

var configFile = "../../config/chainmaker_sdk_config_chain2.yml"

func Test_NewCMLightSDKAdapter(t *testing.T) {
	NewCMLightSDKAdapter("chain2", configFile, logger.GetDefaultLogger())
}

func TestCMLightSDKAdapter_Func(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockSDK := mock.NewMockSDKInterface(ctrl)
	lightSDKAdapter := &CMLightSDKAdapter{
		chainId: "chain1",
		cmSDK: &cmSDK{
			chainClient: mockSDK,
		},
	}
	defer lightSDKAdapter.Stop()
	mockSDK.EXPECT().Stop().Return(nil)
	mockSDK.EXPECT().SubscribeBlock(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().Return(make(chan interface{}, 1), nil)
	mockSDK.EXPECT().GetCurrentBlockHeight().AnyTimes().Return(uint64(1), nil)
	mockSDK.EXPECT().GetBlockByHeight(gomock.Eq(uint64(1)), gomock.Any()).AnyTimes().Return(&cmCommonPb.BlockInfo{
		Block: &cmCommonPb.Block{
			Header: &cmCommonPb.BlockHeader{
				BlockHeight: 1,
			},
		},
	}, nil)
	mockSDK.EXPECT().GetBlockHeaderByHeight(gomock.Any()).AnyTimes().Return(&cmCommonPb.BlockHeader{
		BlockVersion: 2300,
	}, nil)
	mockSDK.EXPECT().GetTxByTxId(gomock.Eq("000")).AnyTimes().Return(&cmCommonPb.TransactionInfo{
		Transaction: &cmCommonPb.Transaction{
			Result: &cmCommonPb.Result{
				Code: cmCommonPb.TxStatusCode_SUCCESS,
			},
		},
	}, nil)
	mockSDK.EXPECT().GetChainConfig().AnyTimes().Return(&config.ChainConfig{
		Crypto: &config.CryptoConfig{
			Hash: "sha256",
		},
	}, nil)

	mockSDK.EXPECT().SendTxRequest(gomock.Any(), gomock.Any(), gomock.Any()).AnyTimes().Return(
		&cmCommonPb.TxResponse{
			Code: cmCommonPb.TxStatusCode_SUCCESS,
		}, nil)

	bCh, err := lightSDKAdapter.SubscribeBlock()
	require.Equal(t, len(bCh), 0)
	require.Nil(t, err)

	height, err := lightSDKAdapter.GetChainHeight()
	require.Equal(t, height, uint64(1))
	require.Nil(t, err)

	blocker, err := lightSDKAdapter.GetBlockByHeight(uint64(1))
	require.Equal(t, blocker.GetHeight(), uint64(1))
	require.Nil(t, err)

	txer, err := lightSDKAdapter.GetTransactionByTxKey("000")
	require.Equal(t, txer.GetStatusCode(), int32(cmCommonPb.TxStatusCode_SUCCESS))
	require.Nil(t, err)

	config, err := lightSDKAdapter.GetChainConfig()
	require.Equal(t, config.HashType, "sha256")
	require.Nil(t, err)

	txReq := &cmCommonPb.TxRequest{
		Payload: &cmCommonPb.Payload{
			ChainId: "chain1",
		},
	}
	resp, err := lightSDKAdapter.ForwardTransaction(txReq)
	require.Nil(t, err)
	require.Equal(t, resp.Code, cmCommonPb.TxStatusCode_SUCCESS)
}
