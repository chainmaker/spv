/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package manager contains block synchronization logic
package manager

import (
	"context"
	"fmt"
	"time"

	"chainmaker.org/chainmaker/spv/v2/adapter"
	"chainmaker.org/chainmaker/spv/v2/coder"
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"chainmaker.org/chainmaker/spv/v2/storage"
	"chainmaker.org/chainmaker/spv/v2/verifier"
	"github.com/Rican7/retry"
	"github.com/Rican7/retry/strategy"
	"github.com/gogo/protobuf/proto"
	"go.uber.org/zap"
)

const (
	defaultBlockInterval = 10 // default block interval, if subscribed block higher than local block
	// more than block interval, discard it
	blockerChanSize                 = 2048  // default size of blockerCh
	heightMsgChanSize               = 1024  // default size of heightMsgCh
	resubscribeInterval             = 10000 // the interval for resubscribing block from remote chain，unit：ms
	defaultSyncRemoteHeightInterval = 10000 // default interval for polling the height of remote chain，unit：ms
)

// StateManager is the block synchronization module
type StateManager struct {
	chainId           string
	reqManger         *ReqManager
	blockManger       *BlockManager
	state             *State
	blockInterval     uint64
	sdkAdapter        adapter.SDKAdapter
	store             storage.StateDB
	blockerCh         chan common.Blocker
	heightMsgCh       chan *HeightMsg
	listenSubscribeCh chan bool
	ctx               context.Context
	cancel            context.CancelFunc
	log               *zap.SugaredLogger
}

// NewStateManager creates StateManger
func NewStateManager(chainId string, blockInterval int32, sdkAdapter adapter.SDKAdapter,
	verifier verifier.Verifier, coder coder.Coder,
	store storage.StateDB, log *zap.SugaredLogger) *StateManager {
	bCh := make(chan common.Blocker, blockerChanSize)
	hCh := make(chan *HeightMsg, heightMsgChanSize)
	ctx, cancel := context.WithCancel(context.Background())
	// new state
	_, height, _ := store.GetLastCommittedBlockHeaderAndHeight(chainId)
	state := NewState(height, height, height)
	// new BlockManager
	bm := NewBlockManager(ctx, chainId, state, verifier, coder, store, bCh, hCh, log)
	// new ReqManager
	rm := NewReqManager(ctx, chainId, state, sdkAdapter, bCh, hCh, log)
	if log == nil {
		log = logger.GetLogger(logger.ModuleStateManager)
	}
	if blockInterval <= 0 {
		blockInterval = defaultBlockInterval
	}
	// new StateManager
	return &StateManager{
		chainId:           chainId,
		reqManger:         rm,
		blockManger:       bm,
		state:             state,
		blockInterval:     uint64(blockInterval),
		sdkAdapter:        sdkAdapter,
		store:             store,
		blockerCh:         bCh,
		heightMsgCh:       hCh,
		listenSubscribeCh: make(chan bool),
		ctx:               ctx,
		cancel:            cancel,
		log:               log,
	}
}

// initRemoteChainConfig queries and writes remote chain config in to db
func (sm *StateManager) initRemoteChainConfig() error {
	// get chain config from remote chain and write into db
	cc := sm.getRemoteChainConfig()
	conf.RemoteChainConfigs[cc.ChainId] = cc
	ccBytes, err := proto.Marshal(cc)
	if err != nil {
		return err
	}
	err = sm.store.WriteChainConfig(sm.chainId, ccBytes)
	if err != nil {
		return fmt.Errorf("[ChainId:%s] write chain config failed! err:%v", sm.chainId, err)
	}
	return nil
}

// getRemoteChainConfig gets chain config by SDK
func (sm *StateManager) getRemoteChainConfig() *protogo.RemoteConfig {
	var (
		err error
		cc  *protogo.RemoteConfig
	)
	err = retry.Retry(func(uint) error {
		cc, err = sm.sdkAdapter.GetChainConfig()
		if err != nil {
			sm.log.Errorf("[ChainId:%s] get chain config failed! err:%v", sm.chainId, err)
			return err
		}
		if cc.ChainId != sm.chainId {
			sm.log.Errorf("[ChainId:%s] inconsistent chainId between chain config and spv config", sm.chainId)
			return err
		}
		return nil
	},
		strategy.Limit(retryCnt),
		strategy.Wait(retryInterval*time.Millisecond),
	)
	if err != nil {
		sm.log.Panicf("[ChainId:%s] get chain config failed! err:%v", sm.chainId, err)
	}
	return cc
}

// Start startups StateManager
func (sm *StateManager) Start() error {
	var err error
	// init remote chain config
	err = sm.initRemoteChainConfig()
	if err != nil {
		sm.log.Errorf("[ChainId:%s] init state manager failed! err:%v", sm.chainId, err)
		return err
	}
	sm.log.Infof("[ChainId:%s] ---- start chain listening and state manager! ----", sm.chainId)
	// subscribe the latest block
	err = sm.SubscribeBlock()
	if err != nil {
		return err
	}
	// monitor and resubscribe block when the connection is broken
	sm.MonitorSubscribeBlock()
	// poll the latest block height of the remote chain periodically
	sm.StartupChainHeightLoop()
	// startup request manager
	sm.reqManger.Start()
	// startup block manager
	sm.blockManger.Start()
	return nil
}

// SubscribeBlock subscribes the latest block from remote chain
func (sm *StateManager) SubscribeBlock() error {
	ch, err := sm.sdkAdapter.SubscribeBlock()
	if err != nil {
		sm.log.Errorf("[ChainId:%s] subscribe block failed! err:%v", sm.chainId, err)
		return err
	}
	sm.log.Infof("[ChainId:%s] subscribe block success!", sm.chainId)
	go func() {
		for {
			select {
			case blocker, ok := <-ch:
				// connection with remote chain is broken and the channel of subscribing block is closed
				if !ok {
					sm.log.Error(fmt.Sprintf("[ChainId:%s] subscribe block channel has been closed", sm.chainId))
					sm.listenSubscribeCh <- true
					return
				}
				// the genesis block is handled separately because the state.currentHeight is set to 0 when the SPV is created
				if blocker.GetHeight() == 0 {
					sm.log.Debugf("[ChainId:%s] receive genesis block by subscribe, block height:%d",
						sm.chainId, blocker.GetHeight())
					sm.blockerCh <- blocker
				}
				currentHeight := sm.state.GetCurrentHeight()
				if blocker.GetHeight() > currentHeight && blocker.GetHeight() <= currentHeight+sm.blockInterval {
					sm.log.Debugf("[ChainId:%s] receive block by subscribe, block height:%d, current height:%d",
						sm.chainId, blocker.GetHeight(), currentHeight)
					sm.blockerCh <- blocker
				}
				remoteHeight := sm.state.GetRemoteMaxHeight()
				if blocker.GetHeight() > remoteHeight {
					err = sm.state.SetRemoteMaxHeight(blocker.GetHeight())
					if err != nil {
						sm.log.Errorf("[ChainId:%s] update remote max height failed! new remote height:%d, old remote height:%d, err:%v",
							sm.chainId, blocker.GetHeight(), remoteHeight, err)
					}
					sm.log.Debugf("[ChainId:%s] update remote height, new remote height:%d, current height:%d, old remote height:%d",
						sm.chainId, blocker.GetHeight(), currentHeight, remoteHeight)
				}
			case <-sm.ctx.Done():
				return
			}
		}
	}()
	return nil
}

// MonitorSubscribeBlock monitors and resubscribes block when the connection is broken
func (sm *StateManager) MonitorSubscribeBlock() {
	go func() {
		for {
			select {
			case <-sm.listenSubscribeCh:
				err := sm.SubscribeBlock()
				if err != nil {
					// resubscribe after 10000ms
					t := time.NewTicker(resubscribeInterval * time.Millisecond)
					// nolint
					select {
					case <-t.C:
						go func() {
							sm.listenSubscribeCh <- true
						}()
					}
				}
			case <-sm.ctx.Done():
				return
			}
		}
	}()
}

// StartupChainHeightLoop polls the latest block height of the remote chain periodically
func (sm *StateManager) StartupChainHeightLoop() {
	var syncInterval int32
	chainConfig, err := conf.SPVConfig.GetChainConfig(sm.chainId)
	if err == nil {
		syncInterval = chainConfig.SyncInterval
	}
	if syncInterval < defaultSyncRemoteHeightInterval {
		syncInterval = defaultSyncRemoteHeightInterval
	}
	// request remote chain height when starting
	sm.RequestChainHeight()
	// poll remote chain height at regular intervals
	t := time.NewTicker(time.Duration(syncInterval) * time.Millisecond)
	go func() {
		for {
			select {
			case <-t.C:
				sm.RequestChainHeight()
			case <-sm.ctx.Done():
				return
			}
		}
	}()
}

// RequestChainHeight requests remote chain height
func (sm *StateManager) RequestChainHeight() {
	remoteHeight, err := sm.sdkAdapter.GetChainHeight()
	if err != nil {
		sm.log.Errorf("[ChainId:%s] request remote chain height failed, err:%v", sm.chainId, err)
	}
	sm.log.Debugf("[ChainId:%s] receive remote chain height:%d, local height:%d, remote max height:%d",
		sm.chainId, remoteHeight, sm.state.GetLocalHeight(), sm.state.GetRemoteMaxHeight())
	// the genesis block is handled separately because the state.requestedMaxHeight is set to 0 when the SPV is created
	if remoteHeight == 0 {
		_, ok := sm.store.GetBlockHeaderByHeight(sm.chainId, 0)
		if !ok {
			sm.log.Debugf("[ChainId:%s] receive gensis block height msg by loop, height:%d", sm.chainId, remoteHeight)
			sm.heightMsgCh <- NewHeightMsg(remoteHeight, true)
		}
	}
	// spv has synced to the highest block
	if remoteHeight == sm.state.GetCurrentHeight() && remoteHeight == sm.state.GetRemoteMaxHeight() {
		sm.log.Infof("[ChainId:%s] spv has synced to the highest block! current local height:%d, remote max height:%d",
			sm.chainId, sm.state.GetCurrentHeight(), sm.state.GetRemoteMaxHeight())
		return
	}
	// spv is behind the remote chain
	if remoteHeight > sm.state.GetLocalHeight() {
		// if state.requestedMaxHeight == 0 && there is not genesis block in spv db, spv need request genesis block again
		if sm.state.GetLocalHeight() == 0 {
			_, ok := sm.store.GetBlockHeaderByHeight(sm.chainId, 0)
			if !ok {
				sm.heightMsgCh <- NewHeightMsg(0, true)
			}
		}
		// spv request the blocks from local height to remoteHeight
		sm.heightMsgCh <- NewHeightMsg(remoteHeight, true)
	}
	if remoteHeight > sm.state.GetRemoteMaxHeight() {
		err = sm.state.SetRemoteMaxHeight(remoteHeight)
		if err != nil {
			sm.log.Errorf("[ChainId:%s] update remote max height failed! remote max height:%d, new height:%d, err:%v",
				sm.chainId, sm.state.GetRemoteMaxHeight(), remoteHeight, err)
		}
	}
}

// GetBlockManager returns block manger
func (sm *StateManager) GetBlockManager() *BlockManager {
	return sm.blockManger
}

// Stop stops StateManager
func (sm *StateManager) Stop() error {
	defer sm.log.Infof("[ChainId:%s] ---- stop chain listening and state manager! ----", sm.chainId)
	err := sm.sdkAdapter.Stop()
	if err != nil {
		sm.log.Error("[ChainId:%s] stop sdk adapter failed!", sm.chainId)
	}
	// close context
	sm.cancel()
	return nil
}

// HeightMsg is the block height information missing from spv
type HeightMsg struct {
	height       uint64
	isFromRemote bool // indicating that the missing height is coming from the remote chain or within the BlockManager
}

// NewHeightMsg create HeightMsg
func NewHeightMsg(height uint64, isRemote bool) *HeightMsg {
	return &HeightMsg{
		height:       height,
		isFromRemote: isRemote,
	}
}
