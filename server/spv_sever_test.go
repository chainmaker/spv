/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package server

import (
	"testing"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/coder/chainmaker_coder"
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/common/chainmaker_common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/manager"
	"chainmaker.org/chainmaker/spv/v2/mock"
	"chainmaker.org/chainmaker/spv/v2/pb/api"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"chainmaker.org/chainmaker/spv/v2/prover/chainmaker_prover"
	"chainmaker.org/chainmaker/spv/v2/storage"
	"chainmaker.org/chainmaker/spv/v2/utils/chainmaker_tools"
	"chainmaker.org/chainmaker/spv/v2/verifier/chainmaker_verifier"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func Test_IntSPVServer(t *testing.T) {
	conf.SPVConfig.StorageConfig = &conf.StoreConfig{
		Provider: "leveldb",
		LevelDB: &conf.LevelDBConfig{
			StorePath:       "../data/spv_db",
			WriteBufferSize: 4,
			BloomFilterBits: 10,
		},
	}

	srv, err := IntSPVServer()
	require.NoError(t, err)
	require.NotNil(t, srv)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	//conf.SPVConfig.StorageConfig.LevelDB.StorePath = "../data/spv_db2"
	srv.spvs["chain1"] = newTestSpv(ctrl, srv.stateDB)
	srv.Start()
	defer srv.Stop()
	conf.RemoteChainConfigs["chain1"] = &protogo.RemoteConfig{
		HashType: "SHA256",
	}

	err = srv.ValidTransaction(&api.TxValidationRequest{
		ChainId:     "chain1",
		TxKey:       "tx1",
		BlockHeight: 2,
		ContractData: &api.ContractData{
			Name:   "name",
			Method: "method",
			Params: []*api.KVPair{
				{
					Key:   "key1",
					Value: []byte("value1"),
				},
			},
		},
		Timeout: 5000,
	})
	require.Nil(t, err)
}

func newTestSpv(ctrl *gomock.Controller, cmStore storage.StateDB) *SPV {
	chainID := "chain1"
	blockVersion := 2300
	cc := &conf.ChainConfig{
		ChainId:       chainID,
		SyncInterval:  1000,
		BlockInterval: 10,
		SDKConfigPath: "../config/chainmaker_sdk_config_chain1.yml",
	}
	conf.SPVConfig.Chains = []*conf.ChainConfig{
		cc,
	}
	conf.RemoteChainConfigs["chain1"] = &protogo.RemoteConfig{
		ChainId:  "chain1",
		HashType: "SHA256",
	}
	tx := &cmCommonPb.Transaction{
		Payload: &cmCommonPb.Payload{
			ChainId:      "chain1",
			ContractName: "name",
			Method:       "method",
			Parameters: []*cmCommonPb.KeyValuePair{
				{
					Key:   "key1",
					Value: []byte("value1"),
				},
			},
		},
	}
	txHash, _ := chainmaker_tools.CalcTxHashWithVersion(tx, blockVersion)

	sdkAdapter := mock.NewMockSDKAdapter(ctrl)
	sdkAdapter.EXPECT().GetChainConfig().Return(&protogo.RemoteConfig{
		ChainId:  "chain1",
		HashType: "HASH256",
	}, nil)
	sdkAdapter.EXPECT().SubscribeBlock().Return(make(chan common.Blocker), nil)
	sdkAdapter.EXPECT().Stop().Return(nil)
	sdkAdapter.EXPECT().GetTransactionByTxKey(gomock.Eq("tx1")).Return(&chainmaker_common.CMTransaction{Transaction: tx, BlockVersion: uint32(blockVersion)}, nil).AnyTimes()
	sdkAdapter.EXPECT().GetChainHeight().Return(uint64(1), nil).AnyTimes()

	bd := storage.NewBlockData([]byte("block Hash"), []byte("chain1 Header"))
	td := &storage.TransactionData{
		TxHashMap: map[string][]byte{
			"tx1": txHash,
			"tx2": []byte("tx2's hash"),
			"tx3": []byte("tx3's hash"),
		},
		TxBytesMap:   nil,
		ExtraDataMap: nil,
	}
	cmStore.CommitBlockDataAndTxData(chainID, 2, bd, td)
	ver := chainmaker_verifier.NewCMSPVVerifier()
	cdr := chainmaker_coder.NewCMSPVCoder()
	cmStateManager := manager.NewStateManager(conf.SPVConfig.Chains[0].ChainId, conf.SPVConfig.Chains[0].BlockInterval, sdkAdapter, ver, cdr, cmStore, nil)
	proverIns := chainmaker_prover.NewChainMakerProver(conf.SPVConfig.Chains[0].ChainId, sdkAdapter, cdr, cmStore, cmStateManager, nil)
	return &SPV{
		adapter:      sdkAdapter,
		verifier:     ver,
		coder:        cdr,
		stateDB:      cmStore,
		stateManager: cmStateManager,
		prover:       proverIns,
	}
}
