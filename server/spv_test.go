/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package server

import (
	"testing"

	"chainmaker.org/chainmaker/spv/v2/coder/chainmaker_coder"
	"chainmaker.org/chainmaker/spv/v2/common"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/manager"
	"chainmaker.org/chainmaker/spv/v2/mock"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo"
	"chainmaker.org/chainmaker/spv/v2/prover/chainmaker_prover"
	"chainmaker.org/chainmaker/spv/v2/storage/kvdb/leveldb"
	"chainmaker.org/chainmaker/spv/v2/verifier/chainmaker_verifier"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
)

func TestSPV_Func(t *testing.T) {
	cc := &conf.ChainConfig{
		ChainId:       "chain1",
		SyncInterval:  1000,
		BlockInterval: 10,
		SDKConfigPath: "../config/chainmaker_sdk_config_chain1.yml",
	}
	conf.SPVConfig = &conf.LocalConfig{
		Chains: []*conf.ChainConfig{
			cc,
		},
		StorageConfig: &conf.StoreConfig{
			Provider: "leveldb",
			LevelDB: &conf.LevelDBConfig{
				StorePath:       "../data/spv_db",
				WriteBufferSize: 4,
				BloomFilterBits: 10,
			},
		},
	}
	store := leveldb.NewKvStateDB(conf.SPVConfig.StorageConfig, nil)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	sdkAdapter := mock.NewMockSDKAdapter(ctrl)
	sdkAdapter.EXPECT().GetChainConfig().Return(&protogo.RemoteConfig{
		ChainId:  "chain1",
		HashType: "HASH256",
	}, nil)
	sdkAdapter.EXPECT().SubscribeBlock().Return(make(chan common.Blocker), nil)
	sdkAdapter.EXPECT().GetChainHeight().Return(uint64(0), nil)
	sdkAdapter.EXPECT().Stop().Return(nil)

	ver := chainmaker_verifier.NewCMSPVVerifier()
	cdr := chainmaker_coder.NewCMSPVCoder()
	cmStateManager := manager.NewStateManager(conf.SPVConfig.Chains[0].ChainId, conf.SPVConfig.Chains[0].BlockInterval, sdkAdapter, ver, cdr, store, nil)
	proverIns := chainmaker_prover.NewChainMakerProver(conf.SPVConfig.Chains[0].ChainId, sdkAdapter, cdr, store, cmStateManager, nil)
	spvIns := &SPV{
		adapter:      sdkAdapter,
		verifier:     ver,
		coder:        cdr,
		stateDB:      store,
		stateManager: cmStateManager,
		prover:       proverIns,
	}
	err := spvIns.Start()
	require.Nil(t, err)
	defer spvIns.Stop()

	aptr := spvIns.GetAdapter()
	require.NotNil(t, aptr)

	cdr2 := spvIns.GetCoder()
	require.NotNil(t, cdr2)

	pvr := spvIns.GetProver()
	require.NotNil(t, pvr)

	db := spvIns.GetStateDB()
	require.NotNil(t, db)

	vfer := spvIns.GetVerifier()
	require.NotNil(t, vfer)

	smr := spvIns.GetStateManager()
	require.NotNil(t, smr)
}
