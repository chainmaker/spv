/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

// Package server is SPV module
package server

import (
	"errors"
	"fmt"

	cmCommonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/spv/v2/adapter"
	"chainmaker.org/chainmaker/spv/v2/adapter/chainmaker_adapter"
	"chainmaker.org/chainmaker/spv/v2/coder"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/manager"
	"chainmaker.org/chainmaker/spv/v2/pb/api"
	"chainmaker.org/chainmaker/spv/v2/pb/protogo/cm_pbgo"
	"chainmaker.org/chainmaker/spv/v2/storage"
	"chainmaker.org/chainmaker/spv/v2/storage/kvdb/leveldb"
	"chainmaker.org/chainmaker/spv/v2/utils/chainmaker_tools"
	"go.uber.org/zap"
)

// SPVServer contains all chains' SPV
type SPVServer struct {
	stateDB storage.StateDB
	spvs    map[string]*SPV // map[chainId]=*SPV
	log     *zap.SugaredLogger
}

// NewSPVServer inits spv config and creates spv server when integrated as a component
func NewSPVServer(ymFile string, log *zap.SugaredLogger) (*SPVServer, error) {
	var server = &SPVServer{
		spvs: make(map[string]*SPV),
	}
	// 1. init spv config
	err := conf.InitSPVConfigWithYmlFile(ymFile)
	if err != nil {
		return nil, err
	}
	if log == nil {
		log = logger.GetDefaultLogger()
	}
	// 2. new store module
	store := leveldb.NewKvStateDB(conf.SPVConfig.StorageConfig, log)
	for _, chain := range conf.SPVConfig.Chains {
		// 3. new spv
		spv, err1 := NewSPV(chain, store, log)
		if err1 != nil {
			return nil, err1
		}
		server.spvs[chain.ChainId] = spv
	}
	server.stateDB = store
	server.log = log
	// 4. return SPVServer
	return server, nil
}

// IntSPVServer inits spv server when deployed independently
func IntSPVServer() (*SPVServer, error) {
	var server = &SPVServer{
		spvs: make(map[string]*SPV),
	}
	// 1. new store module
	store := leveldb.NewKvStateDB(conf.SPVConfig.StorageConfig, nil)
	for _, chain := range conf.SPVConfig.Chains {
		// 2. new spv
		spv, err := NewSPV(chain, store, nil)
		if err != nil {
			return nil, err
		}
		server.spvs[chain.ChainId] = spv
	}
	server.stateDB = store
	server.log = logger.GetLogger(logger.ModuleSpv)
	// 3. return SPVServer
	return server, nil
}

// Start startups all chains' spv
func (ss *SPVServer) Start() error {
	ss.log.Infof("==== Start SPV Server! ==== ")
	for _, spv := range ss.spvs {
		if err := spv.Start(); err != nil {
			return err
		}
	}
	return nil
}

// ValidTransaction verifies the existence and validity of the transaction
func (ss *SPVServer) ValidTransaction(request *api.TxValidationRequest) error {
	if request == nil {
		return fmt.Errorf("TxValidationRequest should not be nil")
	}
	spv, ok := ss.spvs[request.ChainId]
	if !ok {
		ss.log.Errorf("chain id:%s in TxValidationRequest is invalid, when verifying the validity of transaction",
			request.ChainId)
		return fmt.Errorf("chain id:%s in TxValidationRequest is invalid", request.ChainId)
	}
	err := spv.GetProver().VerifyTransaction(request)
	if err != nil {
		ss.log.Warnf("SPVServer verifies an invalid transaction, chainId:%s, txKey:%s, warn:%v",
			request.ChainId, request.TxKey, err)
		return err
	}
	return nil
}

// ForwardRequest Forward Request
func (ss *SPVServer) ForwardRequest(request *cm_pbgo.TxRequest) (*cm_pbgo.TxResponse, error) {
	var (
		resp     = &cmCommonPb.TxResponse{} // nolint
		response = &cm_pbgo.TxResponse{}
		err      error
	)
	if request == nil {
		response.Code = cm_pbgo.TxStatusCode_INTERNAL_ERROR
		response.Message = "TxRequest should not be nil, when forwarding request"
		ss.log.Error(response.Message)
		return response, errors.New(response.Message)
	}
	if request.Payload == nil {
		response.Code = cm_pbgo.TxStatusCode_INTERNAL_ERROR
		response.Message = "TxRequest header should not be nil, when forwarding request"
		ss.log.Error(response.Message)
		return response, errors.New(response.Message)
	}
	spv, ok := ss.spvs[request.Payload.ChainId]
	if !ok {
		response.Code = cm_pbgo.TxStatusCode_INTERNAL_ERROR
		response.Message = "chainId in TxRequest header is invalid, when forwarding request"
		ss.log.Error(response.Message)
		return response, errors.New(response.Message)
	}
	// cm_pbgo.TxRequest to cmCommonPb.TxRequest
	req, err := chainmaker_tools.TxRequestConverter(request)
	if err != nil {
		response.Code = cm_pbgo.TxStatusCode_INTERNAL_ERROR
		response.Message = err.Error()
		ss.log.Errorf("forward request failed, err:%v", err)
		return response, err
	}
	switch apt := spv.GetAdapter().(type) {
	case *chainmaker_adapter.CMSPVSDKAdapter:
		resp, err = apt.ForwardTransaction(req)
		if err != nil {
			response.Code = cm_pbgo.TxStatusCode_INTERNAL_ERROR
			response.Message = err.Error()
			ss.log.Errorf("forward request failed, err:%v", err)
			return response, err
		}
	case *chainmaker_adapter.CMLightSDKAdapter:
		resp, err = apt.ForwardTransaction(req)
		if err != nil {
			response.Code = cm_pbgo.TxStatusCode_INTERNAL_ERROR
			response.Message = err.Error()
			ss.log.Errorf("forward request failed, err:%v", err)
			return response, err
		}
	default:
		response.Code = cm_pbgo.TxStatusCode_INTERNAL_ERROR
		response.Message = "forward request failed, only support forwarding transaction for ChainMaker"
		ss.log.Errorf(response.Message)
		return response, errors.New(response.Message)
	}
	// cmCommonPb.TxResponse to cm_pbgo.TxResponse
	response, err = chainmaker_tools.TxResponseConverter(resp)
	if err != nil {
		response.Code = cm_pbgo.TxStatusCode_INTERNAL_ERROR
		response.Message = err.Error()
		ss.log.Errorf("forward request failed, err:%v", err)
		return response, err
	}
	return response, nil
}

// RegisterCallBack registers CallBack
func (ss *SPVServer) RegisterCallBack(chainId string, callBack manager.CallBack) error {
	spv, ok := ss.spvs[chainId]
	if !ok {
		ss.log.Errorf("register callBack failed, no listening this chain, chainId:%s", chainId)
		return fmt.Errorf("register callBack failed, no listening this chain, chainId:%s", chainId)
	}
	if callBack == nil {
		return errors.New("can not register a nil func")
	}
	return spv.GetStateManager().GetBlockManager().RegisterCallBack(callBack)
}

// Stop stops all chains' spv
func (ss *SPVServer) Stop() error {
	for _, spv := range ss.spvs {
		if err := spv.Stop(); err != nil {
			return err
		}
	}
	ss.stateDB.Close()
	ss.log.Infof("==== Stop SPV Server! ====")
	return nil
}

// GetCoder gets the coder of spv by chain id
func (ss *SPVServer) GetCoder(chainId string) (coder.Coder, error) {
	spv, ok := ss.spvs[chainId]
	if !ok {
		ss.log.Errorf("get conder failed, no listening this chain, chainId:%s", chainId)
		return nil, fmt.Errorf("get conder failed, no listening this chain, chainId:%s", chainId)
	}
	return spv.GetCoder(), nil
}

// GetDB gets the db of spvServer
func (ss *SPVServer) GetDB() storage.StateDB {
	return ss.stateDB
}

// GetAdapter gets the sdk adapter of spv by chain id
func (ss *SPVServer) GetAdapter(chainId string) (adapter.SDKAdapter, error) {
	spv, ok := ss.spvs[chainId]
	if !ok {
		ss.log.Errorf("get adapter failed, no listening this chain, chainId:%s", chainId)
		return nil, fmt.Errorf("get adapter failed, no listening this chain, chainId:%s", chainId)
	}
	return spv.GetAdapter(), nil
}

// SetLog sets log for unit test
func (ss *SPVServer) SetLog(log *zap.SugaredLogger) error {
	if log == nil {
		return errors.New("log is nil")
	}
	ss.log = log
	return nil
}
