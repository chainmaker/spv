/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

 SPDX-License-Identifier: Apache-2.0
*/

package server

import (
	"strings"

	"chainmaker.org/chainmaker/spv/v2/adapter"
	"chainmaker.org/chainmaker/spv/v2/adapter/chainmaker_adapter"
	"chainmaker.org/chainmaker/spv/v2/adapter/fabric_adapter"
	"chainmaker.org/chainmaker/spv/v2/coder"
	"chainmaker.org/chainmaker/spv/v2/coder/chainmaker_coder"
	"chainmaker.org/chainmaker/spv/v2/coder/fabric_coder"
	"chainmaker.org/chainmaker/spv/v2/conf"
	"chainmaker.org/chainmaker/spv/v2/logger"
	"chainmaker.org/chainmaker/spv/v2/manager"
	"chainmaker.org/chainmaker/spv/v2/prover"
	"chainmaker.org/chainmaker/spv/v2/prover/chainmaker_prover"
	"chainmaker.org/chainmaker/spv/v2/prover/fabric_prover"
	"chainmaker.org/chainmaker/spv/v2/storage"
	"chainmaker.org/chainmaker/spv/v2/verifier"
	"chainmaker.org/chainmaker/spv/v2/verifier/chainmaker_verifier"
	"chainmaker.org/chainmaker/spv/v2/verifier/fabric_verifier"
	"go.uber.org/zap"
)

// SPV is a spv instance for a specific remote chain
type SPV struct {
	adapter      adapter.SDKAdapter
	verifier     verifier.Verifier
	coder        coder.Coder
	stateDB      storage.StateDB
	stateManager *manager.StateManager
	prover       prover.Prover
}

// NewSPV creates a spv for a specific remote chain
func NewSPV(chain *conf.ChainConfig, store storage.StateDB, log *zap.SugaredLogger) (*SPV, error) {
	spv := &SPV{
		stateDB: store,
	}
	chainType := strings.ToLower(chain.ChainType)
	switch chainType {
	case conf.ChainMakerSPV:
		spvAdapter, err := chainmaker_adapter.NewCMSPVSDKAdapter(chain.ChainId, chain.SDKConfigPath,
			logger.GetCMSDKLogger())
		if err != nil {
			return nil, err
		}
		spvVerifier := chainmaker_verifier.NewCMSPVVerifier()
		spvCoder := chainmaker_coder.NewCMSPVCoder()
		stateManager := manager.NewStateManager(chain.ChainId, chain.BlockInterval, spvAdapter,
			spvVerifier, spvCoder, store, log)
		prover := chainmaker_prover.NewChainMakerProver(chain.ChainId, spvAdapter, spvCoder, store,
			stateManager, log)

		spv.adapter = spvAdapter
		spv.verifier = spvVerifier
		spv.coder = spvCoder
		spv.stateManager = stateManager
		spv.prover = prover

	case conf.ChainMakerLight:
		lightAdapter, err := chainmaker_adapter.NewCMLightSDKAdapter(chain.ChainId, chain.SDKConfigPath,
			logger.GetCMSDKLogger())
		if err != nil {
			return nil, err
		}
		lightVerifier := chainmaker_verifier.NewCMLightVerifier()
		lightCoder := chainmaker_coder.NewCMLightCoder()
		stateManager := manager.NewStateManager(chain.ChainId, chain.BlockInterval, lightAdapter,
			lightVerifier, lightCoder, store, log)
		// can only validate transactions within its own organization
		prover := chainmaker_prover.NewChainMakerProver(chain.ChainId, lightAdapter, lightCoder, store,
			stateManager, log)

		spv.adapter = lightAdapter
		spv.verifier = lightVerifier
		spv.coder = lightCoder
		spv.stateManager = stateManager
		spv.prover = prover
	case conf.FabricSPV:
		fabAdapter, err := fabric_adapter.NewFabricSDKAdapter(chain.ChainId, chain.SDKConfigPath, chain.FabricConfig.Peers)
		if err != nil {
			return nil, err
		}
		fabVerifier := fabric_verifier.NewFabricVerifier()
		fabCoder := fabric_coder.NewFabricCoder()
		stateManager := manager.NewStateManager(chain.ChainId, chain.BlockInterval,
			fabAdapter, fabVerifier, fabCoder, store, log)
		fabProver := fabric_prover.NewFabricProver(chain.ChainId, fabAdapter, fabCoder, store,
			stateManager, log)

		spv.adapter = fabAdapter
		spv.verifier = fabVerifier
		spv.coder = fabCoder
		spv.stateManager = stateManager
		spv.prover = fabProver
	default:
		panic("Invalid Chain Type! Only Support ChainMaker_Light, ChainMaker_SPV and Fabric_SPV!!!")
	}
	return spv, nil
}

// Start startups a spv instance
func (s *SPV) Start() error {
	err := s.stateManager.Start()
	if err != nil {
		return err
	}
	return nil
}

// Stop stops a spv instance
func (s *SPV) Stop() error {
	err := s.stateManager.Stop()
	if err != nil {
		return err
	}
	return nil
}

// GetAdapter gets adapter
func (s *SPV) GetAdapter() adapter.SDKAdapter {
	return s.adapter
}

// GetVerifier gets verifier
func (s *SPV) GetVerifier() verifier.Verifier {
	return s.verifier
}

// GetCoder gets coder
func (s *SPV) GetCoder() coder.Coder {
	return s.coder
}

// GetStateDB gets stateDB
func (s *SPV) GetStateDB() storage.StateDB {
	return s.stateDB
}

// GetStateManager gets state manager
func (s *SPV) GetStateManager() *manager.StateManager {
	return s.stateManager
}

// GetProver gets prover
func (s *SPV) GetProver() prover.Prover {
	return s.prover
}
